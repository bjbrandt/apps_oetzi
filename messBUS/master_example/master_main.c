/****************************************************************************
 * messBUS/master_main
 *
 *   Author: Peer Ulbig <p.ulbig@tu-braunschweig.de>
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>

#include <nuttx/board.h>

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/boardctl.h>

#include <nuttx/timers/hptc_io.h>
#include <nuttx/timers/hptc.h>

#include <nuttx/messBUS/messBUSMaster.h>

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/* Set this #define to 1 if this app is the application entrypoint in NuttX. */
#define AUTOSTART				1

/* Loop forever and switch slotlists in this loop. Maybe also print the data
 * in the console. */
#define AUTO_SWITCH_SLOTLISTS	1
#define PRINT_DATA				1

/* The following features may only work for a single channel master.
 * When using multiple channels unfortunately a hardfault may occurs:
 * "ERROR: Stack pointer is not within the allocated stack"
 *
 * Wake-up callbacks need further coding for multiple channel masters.
 * Refer to messBUS_config.h.
 */
#define GET_SLOT_NUMBER			0
#define NEXT_SLOT_CALLBACK		0
#define SLOT_NUMBER_CALLBACK	0
#define NEXT_SYNC_CALLBACK		0
#define WAKE_UP_CALLBACK		0



#ifndef NS_PER_SEC
#define NS_PER_SEC 1000000000
#endif


/****************************************************************************
 * Private Data
 ****************************************************************************/

/****************************************************************************
 * Public Functions
 ****************************************************************************/

void mycallback_function(void)
{
	printf("This was a callback!\n");
}

void mywakeup_callback(void)
{
	printf("This was a wake up!\n");
}


/********************************************************
 * sync_task
 *********************************************************/

#define SYNC_VERBOSE

static int sync_task (int argc, char *argv[]) //,int signo
{
  int fd_pps;
  int fd_hptc;

  int ret;


  printf("try to open /dev/pps_in\n");
     fd_pps=open("/dev/pps_in", O_RDWR);
     if (fd_pps < 0)
         {
           printf("ERROR opening /dev/pps_in\n");
           return -1;
         }

   printf("OK\n");

   printf("try to open /dev/hptc\n");
      fd_hptc=open("/dev/hptc", O_RDWR);
      if (fd_pps < 0)
          {
            printf("ERROR opening /dev/hptc\n");
            return -1;
          }


   printf("OK\n");

   struct timespec last_pps;

   //theoretical...
   //kp_krit=1.3
   //T_krit=2.0 s

   //kp=0.6*kp_krit = 0.78
   //Tn=0.5*T_krit  = 1.0
   //Tv=0.12*T_krit = .24

   //ki=kp/Tn = 0.78
   //kd=kp*Tv = 0.18

   //for integer instead of double: multiply by 1000 or use int64_t

   // reality ...
   float e=0;
   float e_alt=e;
   float e_sum=0;
   float kp=0.84;  //0.84
   float ki=0.5;//.5
   float kd=0.1;//.1
   float Ta=1.0; //sample period [s]
   float tune;
   float e_max=200000;

   bool synced=false;

   bool have_last_pps=false;

  #define CONFIG_PPS_PERIOD_NS 1000000000

   for(;;){
   struct hptc_ic_msg_s data;
  	do{
  		printf("x\n");
  		ret=read(fd_pps,&data,sizeof(data));
  	} while (ret<=0);

  	if(have_last_pps==false){
  		/*caught first pps*/
  		printf("first_pps: %9d.%09d\n", data.sec, data.nsec);
  		last_pps.tv_sec=data.sec;
  	    last_pps.tv_nsec=data.nsec;
  		have_last_pps=true;
  		continue;
  	}

  	//check phase:

  	int32_t phase =data.nsec;

  	if(phase > NS_PER_SEC/2){
  		phase -= NS_PER_SEC;
  	}
#ifdef SYNC_VERBOSE
    printf("last: %9d.%09d\n", last_pps.tv_sec, last_pps.tv_nsec);
    printf("pps:  %9d.%09d phase: %d ns\n", data.sec, data.nsec, phase);



#endif
      if(abs(phase)>300000) {

      	//check period between last pps

      	int32_t diff=data.sec-last_pps.tv_sec;
      	if((diff<0) || (diff>2)){
      		/*invalid period*/
      		printf("invalid pps_period last:\n");
      		printf(" last pps: %9d.%09d\n", last_pps.tv_sec, last_pps.tv_nsec);
      		printf("      now: %9d.%09d\n", data.sec, data.nsec);

      		last_pps.tv_sec=data.sec;
      	    last_pps.tv_nsec=data.nsec;

      		continue;
      	}

      	diff*=NS_PER_SEC;
      	diff-=CONFIG_PPS_PERIOD_NS;
      	diff+=(data.nsec-last_pps.tv_nsec);

      	if( (diff<-500000) || (diff > 500000)){
      		/*invalid period*/
      		printf("invalid pps_period: %d ns\n",diff);

      		last_pps.tv_sec=data.sec;
      	    last_pps.tv_nsec=data.nsec;

      		continue;
      	}

        printf("pps_period error: %d ns\n",diff);

#if 1
  		struct timespec jump;

  		jump.tv_sec=0;
  		jump.tv_nsec=NS_PER_SEC-data.nsec;

  		printf("jump: %d ns\n",jump.tv_nsec);


  		ioctl(fd_hptc,HPTCIOC_JUMP,&jump);

  		e_sum=0;
  		e_alt=0;
#endif
  		continue;

      }


  	e=phase;

  	e_sum+=e;
  	tune=(kp*e + ki*Ta*e_sum +kd*(e-e_alt)/Ta);
  	e_alt=e;

  	if(e_sum>e_max){
  		e_sum=e_max;
  	}
  	else if(e_sum<-e_max){
  		e_sum=-e_max;
  	}


  	if (tune> 1e5) tune=1e5;
  	if (tune<-1e5) tune=-1e5;


      int32_t i_tune=tune;

  	ioctl(fd_hptc,HPTCIOC_TUNE,i_tune);
#ifdef SYNC_VERBOSE
  	printf("tune: %7.3f ppm -> %d ppb\n\n", tune/1000.0, i_tune);
#endif

    }

    return 0;








}
/****************************************************************************
 * master_main
 ****************************************************************************/

#ifdef CONFIG_BUILD_KERNEL
int main(int argc, FAR char *argv[])
#else
int master_main(int argc, char *argv[])
#endif
{
  int fd;
  int ret;
  int errcode;
  int counter = 0;
  int activeslotlist = 0;
  struct slotlists_container_s container;
  uint8_t slot_number = 0;
  struct single_callback_spec_s single_callback;
  struct wake_up_callback_spec_s wake_up_callback;

  char rxbuffer0[20] = "Err";
  char rxbuffer1[20] = "Err";
  char txbuffer0[20] = "MASTER";
  char txbuffer1[20] = "Ma1";
  uint16_t buflength0 = 6;
  uint16_t buflength1 = 3;

#if CONFIG_MESSBUS_USE_CH1
  ch1_slotlist_s slotlist10 = {0};
  ch1_slotlist_s slotlist11 = {0};
#endif
#if CONFIG_MESSBUS_USE_CH2
  ch2_slotlist_s slotlist20 = {0};
  ch2_slotlist_s slotlist21 = {0};
#endif
#if CONFIG_MESSBUS_USE_CH3
  ch3_slotlist_s slotlist30 = {0};
  ch3_slotlist_s slotlist31 = {0};
#endif
#if CONFIG_MESSBUS_USE_CH4
  ch4_slotlist_s slotlist40 = {0};
  ch4_slotlist_s slotlist41 = {0};
#endif

#if CONFIG_MESSBUS_USE_CH1
 /* Slotlist10 containing Slots 0-3 */
  slotlist10.n_slots = 4;
  slotlist10.slot[0].baud = 4000000;
  slotlist10.slot[0].buf_length = buflength0;
  slotlist10.slot[0].buffer = txbuffer0;
  slotlist10.slot[0].read_write = MESSBUS_TX;
  slotlist10.slot[0].t_start = 10;
  slotlist10.slot[0].t_end = 28;

  slotlist10.slot[1].baud = 10000000;
  slotlist10.slot[1].buf_length = 0;
  slotlist10.slot[1].buffer = rxbuffer0;
  slotlist10.slot[1].read_write = MESSBUS_RX;
  slotlist10.slot[1].t_start = 100;//32;
  slotlist10.slot[1].t_end = 200;//40;

  slotlist10.slot[2].baud = 4000000;
  slotlist10.slot[2].buf_length = 0;
  slotlist10.slot[2].buffer = rxbuffer1;
  slotlist10.slot[2].read_write = MESSBUS_RX;
  slotlist10.slot[2].t_start = 300;//45;
  slotlist10.slot[2].t_end = 400;//51;

  slotlist10.slot[3].baud = 10000000;
  slotlist10.slot[3].buf_length = buflength1;
  slotlist10.slot[3].buffer = txbuffer1;
  slotlist10.slot[3].read_write = MESSBUS_TX;
  slotlist10.slot[3].t_start = 500;//56;
  slotlist10.slot[3].t_end = 600;//62;

  /* Slotlist11 containing Slots 0-3 */
  slotlist11.n_slots = 4;
  slotlist11.slot[0].baud = 10000000;
  slotlist11.slot[0].buf_length = buflength0;
  slotlist11.slot[0].buffer = txbuffer0;
  slotlist11.slot[0].read_write = MESSBUS_TX;
  slotlist11.slot[0].t_start = 10;
  slotlist11.slot[0].t_end = 28;

  slotlist11.slot[1].baud = 10000000;
  slotlist11.slot[1].buf_length = 0;
  slotlist11.slot[1].buffer = rxbuffer0;
  slotlist11.slot[1].read_write = MESSBUS_RX;
  slotlist11.slot[1].t_start = 100;//32;
  slotlist11.slot[1].t_end = 200;//40;

  slotlist11.slot[2].baud = 4000000;
  slotlist11.slot[2].buf_length = 0;
  slotlist11.slot[2].buffer = rxbuffer1;
  slotlist11.slot[2].read_write = MESSBUS_RX;
  slotlist11.slot[2].t_start = 300;//45;
  slotlist11.slot[2].t_end = 400;//51;

  slotlist11.slot[3].baud = 10000000;
  slotlist11.slot[3].buf_length = buflength1;
  slotlist11.slot[3].buffer = txbuffer1;
  slotlist11.slot[3].read_write = MESSBUS_TX;
  slotlist11.slot[3].t_start = 500;//56;
  slotlist11.slot[3].t_end = 600;//62;
#endif

#if CONFIG_MESSBUS_USE_CH2
 /* Slotlist20 containing Slots 0-3 */
  slotlist20.n_slots = 4;
  slotlist20.slot[0].baud = 4000000;
  slotlist20.slot[0].buf_length = buflength0;
  slotlist20.slot[0].buffer = txbuffer0;
  slotlist20.slot[0].read_write = MESSBUS_TX;
  slotlist20.slot[0].t_start = 10;
  slotlist20.slot[0].t_end = 28;

  slotlist20.slot[1].baud = 10000000;
  slotlist20.slot[1].buf_length = 0;
  slotlist20.slot[1].buffer = rxbuffer0;
  slotlist20.slot[1].read_write = MESSBUS_RX;
  slotlist20.slot[1].t_start = 100;//32;
  slotlist20.slot[1].t_end = 200;//40;

  slotlist20.slot[2].baud = 4000000;
  slotlist20.slot[2].buf_length = 0;
  slotlist20.slot[2].buffer = rxbuffer1;
  slotlist20.slot[2].read_write = MESSBUS_RX;
  slotlist20.slot[2].t_start = 300;//45;
  slotlist20.slot[2].t_end = 400;//51;

  slotlist20.slot[3].baud = 10000000;
  slotlist20.slot[3].buf_length = buflength1;
  slotlist20.slot[3].buffer = txbuffer1;
  slotlist20.slot[3].read_write = MESSBUS_TX;
  slotlist20.slot[3].t_start = 500;//56;
  slotlist20.slot[3].t_end = 600;//62;

  /* Slotlist21 containing Slots 0-3 */
  slotlist21.n_slots = 4;
  slotlist21.slot[0].baud = 10000000;
  slotlist21.slot[0].buf_length = buflength0;
  slotlist21.slot[0].buffer = txbuffer0;
  slotlist21.slot[0].read_write = MESSBUS_TX;
  slotlist21.slot[0].t_start = 10;
  slotlist21.slot[0].t_end = 28;

  slotlist21.slot[1].baud = 10000000;
  slotlist21.slot[1].buf_length = 0;
  slotlist21.slot[1].buffer = rxbuffer0;
  slotlist21.slot[1].read_write = MESSBUS_RX;
  slotlist21.slot[1].t_start = 100;//32;
  slotlist21.slot[1].t_end = 200;//40;

  slotlist21.slot[2].baud = 4000000;
  slotlist21.slot[2].buf_length = 0;
  slotlist21.slot[2].buffer = rxbuffer1;
  slotlist21.slot[2].read_write = MESSBUS_RX;
  slotlist21.slot[2].t_start = 300;//45;
  slotlist21.slot[2].t_end = 400;//51;

  slotlist21.slot[3].baud = 10000000;
  slotlist21.slot[3].buf_length = buflength1;
  slotlist21.slot[3].buffer = txbuffer1;
  slotlist21.slot[3].read_write = MESSBUS_TX;
  slotlist21.slot[3].t_start = 500;//56;
  slotlist21.slot[3].t_end = 600;//62;
#endif

/* Channel 3 not fully supported yet. */

#if CONFIG_MESSBUS_USE_CH4
 /* Slotlist40 containing Slots 0-3 */
	slotlist40.n_slots = 4;
  slotlist40.slot[0].baud = 4000000;
  slotlist40.slot[0].buf_length = buflength0;
  slotlist40.slot[0].buffer = txbuffer0;
  slotlist40.slot[0].read_write = MESSBUS_TX;
	slotlist40.slot[0].t_start = 10;
	slotlist40.slot[0].t_end = 28;

	slotlist40.slot[1].baud = 10000000;
  slotlist40.slot[1].buf_length = 0;
  slotlist40.slot[1].buffer = rxbuffer0;
  slotlist40.slot[1].read_write = MESSBUS_RX;
	slotlist40.slot[1].t_start = 100; //32;
	slotlist40.slot[1].t_end = 200; //40;

  slotlist40.slot[2].baud = 4000000;
  slotlist40.slot[2].buf_length = 0;
  slotlist40.slot[2].buffer = rxbuffer1;
  slotlist40.slot[2].read_write = MESSBUS_RX;
  slotlist40.slot[2].t_start = 300;//45;
  slotlist40.slot[2].t_end = 400;//51;

  slotlist40.slot[3].baud = 4000000;
  slotlist40.slot[3].buf_length = buflength1;
  slotlist40.slot[3].buffer = txbuffer1;
  slotlist40.slot[3].read_write = MESSBUS_TX;
  slotlist40.slot[3].t_start = 500;//56;
  slotlist40.slot[3].t_end = 600;//62;

  /* Slotlist41 containing Slots 0-3 */
	slotlist41.n_slots = 4;
  slotlist41.slot[0].baud = 4000000;
  slotlist41.slot[0].buf_length = buflength0;
  slotlist41.slot[0].buffer = txbuffer0;
  slotlist41.slot[0].read_write = MESSBUS_TX;
  slotlist41.slot[0].t_start = 10;
  slotlist41.slot[0].t_end = 28;

	slotlist41.slot[1].baud = 10000000;
  slotlist41.slot[1].buf_length = 0;
  slotlist41.slot[1].buffer = rxbuffer0;
  slotlist41.slot[1].read_write = MESSBUS_RX;
	slotlist41.slot[1].t_start = 100; //32;
	slotlist41.slot[1].t_end = 200; //40;

  slotlist41.slot[2].baud = 4000000;
  slotlist41.slot[2].buf_length = 0;
  slotlist41.slot[2].buffer = rxbuffer1;
  slotlist41.slot[2].read_write = MESSBUS_RX;
  slotlist41.slot[2].t_start = 300;//45;
  slotlist41.slot[2].t_end = 400;//51;

	slotlist41.slot[3].baud = 4000000;
  slotlist41.slot[3].buf_length = buflength1;
  slotlist41.slot[3].buffer = txbuffer1;
  slotlist41.slot[3].read_write = MESSBUS_TX;
  slotlist41.slot[3].t_start = 500;//56;
  slotlist41.slot[3].t_end = 600;//62;
#endif

  /* Attach slotlist0 to the slotlist container */
#if CONFIG_MESSBUS_USE_CH1
  container.ch1_slotlist = &slotlist10;
#endif
#if CONFIG_MESSBUS_USE_CH2
  container.ch2_slotlist = &slotlist20;
#endif
#if CONFIG_MESSBUS_USE_CH3
  container.ch3_slotlist = &slotlist30;
#endif
#if CONFIG_MESSBUS_USE_CH4
  container.ch4_slotlist = &slotlist40;
#endif


printf("\n\nmessBUS Example\n\n");
  /* If this app is selected as CONFIG_USER_ENTRYPOINT the registration
   * of the driver may happens too late for the F7 boards under NuttX.
   * A quick and dirty workaround is to call the boardioc_init command
   * ourselves.
   */
//#if defined(CONFIG_ARCH_CHIP_STM32F7) && AUTOSTART
//  (void)boardctl(BOARDIOC_INIT, 0);
//#endif

sleep(1);

/*synchronization task*/
#if 1
ret = task_create("sync_task",101,2048,sync_task, NULL);
sleep(10);
#endif

//#define USB_TEST

#ifdef USB_TEST
struct boardioc_usbdev_ctrl_s ctrl;
FAR void *handle;
int svalue;

/* Initialize the USB serial driver */

 #if defined(CONFIG_PL2303) || defined(CONFIG_CDCACM)
 #ifdef CONFIG_CDCACM

   ctrl.usbdev   = BOARDIOC_USBDEV_CDCACM;
   ctrl.action   = BOARDIOC_USBDEV_CONNECT;
   ctrl.instance = 0;
   ctrl.handle   = &handle;

 #else

  ctrl.usbdev   = BOARDIOC_USBDEV_PL2303;
  ctrl.action   = BOARDIOC_USBDEV_CONNECT;
  ctrl.instance = 0;
  ctrl.handle   = &handle;

 #endif

   ret = boardctl(BOARDIOC_USBDEV_CONTROL, (uintptr_t)&ctrl);
   UNUSED(ret); /* Eliminate warning if not used */
   DEBUGASSERT(ret == OK);
 #endif


   int fd_usb;

	do{
		fd_usb = open("/dev/ttyACM0", O_RDWR);
		if (fd_usb < 0)
		{
			DEBUGASSERT(errno == ENOTCONN);

			printf("USB not connected\n");
			sleep(2);
		}else{
			  	printf("USB connected\n");
		}
	}while (fd_usb < 0);


#define TESTBUFSIZE 256
char buffer[TESTBUFSIZE];
for (int i=0;i<TESTBUFSIZE;i++){
	buffer[i]='0'+(i%10);
}
buffer[0]='X';

buffer[TESTBUFSIZE-2]='\r';
buffer[TESTBUFSIZE-1]='\n';

for(int i=9;i>=0;i--){
	sleep(1);
	printf ("start writing in %ds.\r",i);
}

printf("\n writing to USB ...\n");

#if 1
for(;;)
{
//	printf("write buffer\n");
	write(fd_usb,buffer,sizeof(buffer));
//	usleep(10);
//	sleep(1);
}
#endif

#endif

struct timespec ts;
clock_gettime(CLOCK_REALTIME, &ts);
printf("time: %d.%09d\n",ts.tv_sec,ts.tv_nsec);

printf("try to open /dev/messBusMaster\n");

  /* Open the messBUSClient device and save the file descriptor */
  fd = open("/dev/messBusMaster", O_RDWR);
  if (fd < 0)
      {
        printf("ERROR opening /dev/messBusMaster\n");
        return -1;
      }

printf("OK\n");


#if 0
for (;;){
clock_gettime(CLOCK_REALTIME, &ts);
printf("time: %d.%09d\n",ts.tv_sec,ts.tv_nsec);
usleep(500000);
}
#endif

#if WAKE_UP_CALLBACK
  /* Configure the wake up callback before we attach the slotlist.
   * Configuration of wake up callbacks is only effective after a
   * call to  MESSBUSIOC_ATTACH_SLOTLISTS */
  wake_up_callback.t_wake_up = 9950; // [us]
  wake_up_callback.callback_function = &mywakeup_callback;
#if CONFIG_MESSBUS_USE_CH1
  ret = ioctl(fd, MESSBUSIOC_CH1_CONFIGURE_WAKE_UP_CALLBACK, (unsigned long) &wake_up_callback);
  if (ret < 0)
	  {
	  errcode = errno;
	  printf("hello_main: Ioctl configure wake up callback failed: %d\n", errcode);
	  }

  /* Also enable the wake up callback */
  ret = ioctl(fd, MESSBUSIOC_CH1_ENABLE_WAKE_UP_CALLBACK, (unsigned long) 0);
    if (ret < 0)
  	  {
  	  errcode = errno;
  	  printf("hello_main: Ioctl enable wake up callback failed: %d\n", errcode);
  	  }
#endif
#if CONFIG_MESSBUS_USE_CH2
  ret = ioctl(fd, MESSBUSIOC_CH2_CONFIGURE_WAKE_UP_CALLBACK, (unsigned long) &wake_up_callback);
  if (ret < 0)
	  {
	  errcode = errno;
	  printf("hello_main: Ioctl configure wake up callback failed: %d\n", errcode);
	  }

  /* Also enable the wake up callback */
  ret = ioctl(fd, MESSBUSIOC_CH2_ENABLE_WAKE_UP_CALLBACK, (unsigned long) 0);
    if (ret < 0)
  	  {
  	  errcode = errno;
  	  printf("hello_main: Ioctl enable wake up callback failed: %d\n", errcode);
  	  }
#endif
#if CONFIG_MESSBUS_USE_CH4
  ret = ioctl(fd, MESSBUSIOC_CH4_CONFIGURE_WAKE_UP_CALLBACK, (unsigned long) &wake_up_callback);
  if (ret < 0)
	  {
	  errcode = errno;
	  printf("hello_main: Ioctl configure wake up callback failed: %d\n", errcode);
	  }

  /* Also enable the wake up callback */
  ret = ioctl(fd, MESSBUSIOC_CH4_ENABLE_WAKE_UP_CALLBACK, (unsigned long) 0);
    if (ret < 0)
  	  {
  	  errcode = errno;
  	  printf("hello_main: Ioctl enable wake up callback failed: %d\n", errcode);
  	  }
#endif
#endif //WAKE_UP_CALLBACK

  printf("attach slotlist\n");
  /* Attach a slotlist container to the file descriptor (which
   * represents our messBUSClient device) via ioctl.
   */
  ret = ioctl(fd, MESSBUSIOC_ATTACH_SLOTLISTS, (unsigned long) &container);
  if (ret < 0)
	  {
	  errcode = errno;
	  printf("master_main: Ioctl slotlists failed: %d\n", errcode);
	  }

  printf("Start the master\n");
   /* Start the messBUS via ioctl.
    * Starting the messBUS always requires a previous call of the ioctl
    * command MESSBUSIOC_ATTACH_SLOTLISTS. Otherwise the ioctl command
    * MESSBUSIOC_START will fail, because the messBUS does not know what
    * to do when running.
    */
   ret = ioctl(fd, MESSBUSIOC_START, 0);
   if (ret < 0)
      {
   	  errcode = errno;
   	  printf("master_main: Ioctl start failed: %d\n", errcode);
   	  }

   printf("loop...\n");
   /* Now loop forever */
   for(;;)
   {
#if AUTO_SWITCH_SLOTLISTS
	/* Wait until a new time slice has begun.
	 * In order not to miss new time slices CONFIG_USEC_PER_TICK should not
	 * exceed 1000 (1ms) by much as this ioctl command is in fact a while
	 * loop calling usleep(CONFIG_MESSBUS_SYNCWAIT_SLEEP) until a new
	 * time slice has begun.
	 */
	ret = ioctl(fd, MESSBUSIOC_SYNC_BUSYWAIT, 0);
	if (ret < 0)
	     {
	   	 errcode = errno;
	   	 printf("master_main: Ioctl syncwait failed: %d\n", errcode);
	   	 }
	else
		counter++;

	/* After 100 timeslices change the slotlist again and reset the counter */
	if (counter > 99)
	{
		counter = 0;
		if (activeslotlist)
		{
			#if CONFIG_MESSBUS_USE_CH1
			  container.ch1_slotlist = &slotlist10;
			#endif
			#if CONFIG_MESSBUS_USE_CH2
			  container.ch2_slotlist = &slotlist20;
			#endif
			#if CONFIG_MESSBUS_USE_CH3
			  container.ch3_slotlist = &slotlist30;
			#endif
			#if CONFIG_MESSBUS_USE_CH4
			  container.ch4_slotlist = &slotlist40;
			#endif
			activeslotlist = 0;
		}
		else
		{
			#if CONFIG_MESSBUS_USE_CH1
			  container.ch1_slotlist = &slotlist11;
			#endif
			#if CONFIG_MESSBUS_USE_CH2
			  container.ch2_slotlist = &slotlist21;
			#endif
			#if CONFIG_MESSBUS_USE_CH3
			  container.ch3_slotlist = &slotlist31;
			#endif
			#if CONFIG_MESSBUS_USE_CH4
			  container.ch4_slotlist = &slotlist41;
			#endif
			activeslotlist = 1;
		}

		/* Attach the new slotlist container to the messBUS device */
		ret = ioctl(fd, MESSBUSIOC_ATTACH_SLOTLISTS, (unsigned long) &container);
		if (ret < 0)
			  {
			  errcode = errno;
			  printf("master_main: Ioctl slotlists failed: %d\n", errcode);
			  }
	}

	/* If using a STM32F7 architecture always call the ioctl command
	 * MESSBUSIOC_UPDATE_RXBUFFERS before accessing the rx buffers.
	 * This has to be done because the CPU's cache might not be
	 * coherent with the new data of the rxbuffers in the SRAM.
	 *
	 * You can also call this ioctl command for other architectures,
	 * but it will have no effect then.
	 */
	ret = ioctl(fd, MESSBUSIOC_UPDATE_RXBUFFERS, 0);
	if (ret < 0)
		 {
		 errcode = errno;
		 printf("master_main: Ioctl cache failed: %d\n", errcode);
		 }

#if GET_SLOT_NUMBER
#if CONFIG_MESSBUS_USE_CH1
	/* For example busy wait for slot 1 being processed */
	while(slot_number != 1)
	{
	ret = ioctl(fd, MESSBUSIOC_CH1_GET_LAST_PROCESSED_SLOT_NUMBER, (unsigned long) &slot_number);
	if (ret < 0)
		 {
		 errcode = errno;
		 printf("hello_main: Ioctl get slot number failed: %d\n", errcode);
		 }
	}
	printf("Slot: %d \n", slot_number);
	slot_number = 10;
#endif
#if CONFIG_MESSBUS_USE_CH2
	/* For example busy wait for slot 2 being processed */
	while(slot_number != 2)
	{
	ret = ioctl(fd, MESSBUSIOC_CH2_GET_LAST_PROCESSED_SLOT_NUMBER, (unsigned long) &slot_number);
	if (ret < 0)
		 {
		 errcode = errno;
		 printf("hello_main: Ioctl get slot number failed: %d\n", errcode);
		 }
	}
	printf("Slot: %d \n", slot_number);
	slot_number = 10;
#endif
#if CONFIG_MESSBUS_USE_CH4
	ret = ioctl(fd, MESSBUSIOC_CH4_GET_LAST_PROCESSED_SLOT_NUMBER, (unsigned long) &slot_number);
	if (ret < 0)
		 {
		 errcode = errno;
		 printf("hello_main: Ioctl get slot number failed: %d\n", errcode);
		 }
	printf("Slot: %d \n", slot_number);
#endif
#endif

#if NEXT_SLOT_CALLBACK
#if CONFIG_MESSBUS_USE_CH1
	ret = ioctl(fd, MESSBUSIOC_CH1_ATTACH_NEXT_SLOT_CALLBACK, (unsigned long) &mycallback_function);
	if (ret < 0)
		 {
		 errcode = errno;
		 printf("hello_main: Ioctl next slot callback failed: %d\n", errcode);
		 }
	else
	{
		printf("Callback for next slot attached!\n");
	}
#endif
#if CONFIG_MESSBUS_USE_CH2
	ret = ioctl(fd, MESSBUSIOC_CH2_ATTACH_NEXT_SLOT_CALLBACK, (unsigned long) &mycallback_function);
	if (ret < 0)
		 {
		 errcode = errno;
		 printf("hello_main: Ioctl next slot callback failed: %d\n", errcode);
		 }
	else
	{
		printf("Callback for next slot attached!\n");
	}
#endif
#if CONFIG_MESSBUS_USE_CH4
	ret = ioctl(fd, MESSBUSIOC_CH4_ATTACH_NEXT_SLOT_CALLBACK, (unsigned long) &mycallback_function);
	if (ret < 0)
		 {
		 errcode = errno;
		 printf("hello_main: Ioctl next slot callback failed: %d\n", errcode);
		 }
	else
	{
		printf("Callback for next slot attached!\n");
	}
#endif
#endif

#if SLOT_NUMBER_CALLBACK
	single_callback.callback_function = &mycallback_function;
	single_callback.slot_number = 3;
#if CONFIG_MESSBUS_USE_CH1
	ret = ioctl(fd, MESSBUSIOC_CH1_ATTACH_SLOT_NUMBER_CALLBACK, (unsigned long) &single_callback);
	if (ret < 0)
		 {
		 errcode = errno;
		 printf("hello_main: Ioctl slot number callback failed: %d\n", errcode);
		 }
	else
	{
		printf("Callback for slot %d attached!\n", single_callback.slot_number);
	}
#endif
#if CONFIG_MESSBUS_USE_CH2
	ret = ioctl(fd, MESSBUSIOC_CH2_ATTACH_SLOT_NUMBER_CALLBACK, (unsigned long) &single_callback);
	if (ret < 0)
		 {
		 errcode = errno;
		 printf("hello_main: Ioctl slot number callback failed: %d\n", errcode);
		 }
	else
	{
		printf("Callback for slot %d attached!\n", single_callback.slot_number);
	}
#endif
#if CONFIG_MESSBUS_USE_CH4
	ret = ioctl(fd, MESSBUSIOC_CH4_ATTACH_SLOT_NUMBER_CALLBACK, (unsigned long) &single_callback);
	if (ret < 0)
		 {
		 errcode = errno;
		 printf("hello_main: Ioctl slot number callback failed: %d\n", errcode);
		 }
	else
	{
		printf("Callback for slot %d attached!\n", single_callback.slot_number);
	}
#endif
#endif

#if NEXT_SYNC_CALLBACK
#if CONFIG_MESSBUS_USE_CH1
	ret = ioctl(fd, MESSBUSIOC_CH2_ATTACH_NEXT_SYNC_CALLBACK, (unsigned long) &mycallback_function);
	if (ret < 0)
		 {
		 errcode = errno;
		 printf("hello_main: Ioctl sync callback failed: %d\n", errcode);
		 }
	else
	{
		printf("Callback for next sync attached!\n");
	}
#endif
#if CONFIG_MESSBUS_USE_CH2
	ret = ioctl(fd, MESSBUSIOC_CH2_ATTACH_NEXT_SYNC_CALLBACK, (unsigned long) &mycallback_function);
	if (ret < 0)
		 {
		 errcode = errno;
		 printf("hello_main: Ioctl sync callback failed: %d\n", errcode);
		 }
	else
	{
		printf("Callback for next sync attached!\n");
	}
#endif
#if CONFIG_MESSBUS_USE_CH4
	ret = ioctl(fd, MESSBUSIOC_CH4_ATTACH_NEXT_SYNC_CALLBACK, (unsigned long) &mycallback_function);
	if (ret < 0)
		 {
		 errcode = errno;
		 printf("hello_main: Ioctl sync callback failed: %d\n", errcode);
		 }
	else
	{
		printf("Callback for next sync attached!\n");
	}
#endif
#endif

#if PRINT_DATA

	clock_gettime(CLOCK_REALTIME, &ts);
	printf("time: %9d.%09d\n",ts.tv_sec,ts.tv_nsec);

	/* Printf our data */
//	printf("%d\n", counter);
//	printf("%s \n", rxbuffer0);
//	printf("%s \n", rxbuffer1);
		printf("%d \t", slotlist40.slot[1].buf_length);
		printf("%d \n", slotlist41.slot[1].buf_length);
#endif
#endif // AUTO_SWITCH_SLOTLISTS
   }

/* We should never reach this point */

  return 0;
}
