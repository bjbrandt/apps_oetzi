/****************************************************************************
 * examples/usb_example/usb_example_main.c
 *
 *   Copyright (C) 2008, 2011-2012 Gregory Nutt. All rights reserved.
 *   Author: Gregory Nutt <gnutt@nuttx.org>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>
#include <stdio.h>


#include <sys/boardctl.h>
#include <sys/ioctl.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <sched.h>
#include <errno.h>

#include <nuttx/arch.h>

#include <nuttx/usb/cdcacm.h>
/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * TASK Functions
 ****************************************************************************/

static int usb_example_deamon(int argc, char *argv[])
{
	int fd;
	int ret;
	int nlc;
	int nbytes;
	char readbyte;
	char mybuffer[25] = "USB READY PRESS P \n\r";
	char usbtx [21] = "Detected P\n \r";



	do{
		fd = open("/dev/ttyACM0", O_RDWR);
		if (fd < 0)
		{
			DEBUGASSERT(errno == ENOTCONN);

			printf("USB not connected\n");
			sleep(2);
			}else{
			  	printf("USB connected\n");
			}
	}while (fd < 0);

	/*USB MESSAGE WELCOME*/
	ret = write(fd,&mybuffer,sizeof(mybuffer));



	/*READ ONE BYTE*/
	do{
		readbyte = 0;
		nbytes = read(fd, &readbyte, 1);

		if(nbytes == 1)  {
			printf("Received: %c \n",readbyte);
		}
		if (nbytes == 1 && (readbyte == 'P'))
		{
			//Yes.. increment the count

			nlc = 3;
		}else{
			nlc = 0;
		}
	}while (nlc < 3);

	/*USB MESSAGE ANSWEAR*/
	ret = write(fd,&usbtx,sizeof(usbtx));

	printf("1: %c ;  \n",mybuffer);

	close(fd);
	printf("%USB connection fin\n");

	return 0;
}


/****************************************************************************
 * usb_example_main
 ****************************************************************************/

#ifdef CONFIG_BUILD_KERNEL
int main(int argc, FAR char *argv[])
#else
int usb_example_main(int argc, char *argv[])
#endif
{
	struct boardioc_usbdev_ctrl_s ctrl;
	FAR void *handle;
	int ret;
	int svalue;

	/* Initialize the USB serial driver */

	 #if defined(CONFIG_PL2303) || defined(CONFIG_CDCACM)
	 #ifdef CONFIG_CDCACM

	   ctrl.usbdev   = BOARDIOC_USBDEV_CDCACM;;
	   ctrl.action   = BOARDIOC_USBDEV_CONNECT;
	   ctrl.instance = 0;
	   ctrl.handle   = &handle;

	 #else

	  ctrl.usbdev   = BOARDIOC_USBDEV_PL2303;
	  ctrl.action   = BOARDIOC_USBDEV_CONNECT;
	  ctrl.instance = CONFIG_NSH_USBDEV_MINOR;
	  ctrl.handle   = &handle;

	 #endif

	   ret = boardctl(BOARDIOC_USBDEV_CONTROL, (uintptr_t)&ctrl);
	   UNUSED(ret); /* Eliminate warning if not used */
	   DEBUGASSERT(ret == OK);
	 #endif


	 /*Start TX/RX TASK*/

	 ret = task_create("usb_example_daemon", CONFIG_EXAMPLES_USB_EXAMPLE_PRIORITY,
			   	   	   	  CONFIG_EXAMPLES_USB_EXAMPLE_STACKSIZE, usb_example_deamon,
	   		   		      NULL);

	 printf("RETURN TASK USB_EXAMPLE: %i \n",ret);

  return 0;
}
