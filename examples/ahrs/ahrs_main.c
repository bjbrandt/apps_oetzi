/****************************************************************************
 * examples/ahrs/ahrs_main.c
 *
 *   Copyright (C) 2008, 2011-2012 Gregory Nutt. All rights reserved.
 *   Author: Gregory Nutt <gnutt@nuttx.org>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>
#include <stdio.h>
#include <nuttx/sensors/adis16xxx.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <math.h>
/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * hello_main
 ****************************************************************************/

#ifdef CONFIG_BUILD_KERNEL
int main(int argc, FAR char *argv[])
#else
int ahrs_main(int argc, char *argv[])
#endif
{
	int fd;

	struct adis16488_msg_s data;
	int ret;
	//long test = 0;

	printf("open /dev/imu0\n");
	fd = open("/dev/imu0",O_RDWR | O_NONBLOCK);

	if(fd<0){
		printf("error opening /dev/imu0\n");
		return -1;
	}

	printf("opened /dev/imu0\n");

#define SAMPLE_LENGTH 2500
#define INIT_CNT 10000

	int64_t iax;
	int64_t iay;
	int64_t iaz;

	int64_t iomx,iomy,iomz=0;
	int64_t imagx,imagy,imagz=0;
	int64_t ip=0;
	int32_t iT=0;

	double angle[3] = {0.0, 0.0, 0.0};
	double omega_offset[3] = {0.0, 0.0, 0.0};
	double angle_dot[3];
	double angle_last[3];
	double phi,theta,psi;
	double omx=0.0,omy=0.0,omz=0.0;
	double omx_offset=0.0,omy_offset=0.0,omz_offset=0.0;

	int32_t last_s=0;
	int32_t last_ns=0;
	int32_t delta_ns=0;

	int init_cnt=INIT_CNT;
	int sample_cnt=0;

	while(1){

		do{
			ret=read(fd,&data,sizeof(data));
		} while (ret<=0);


		delta_ns=(data.timestamp_s-last_s)*1000000000 + (data.timestamp_ns-last_ns);
		last_s=data.timestamp_s;
		last_ns=data.timestamp_ns;


		iax+=data.x_accl;
		iay+=data.y_accl;
		iaz+=data.z_accl;

		iomx+=data.x_gyro;
		iomy+=data.y_gyro;
		iomz+=data.z_gyro;

		imagx+=data.x_mag;
		imagy+=data.y_mag;
		imagz+=data.z_mag;

		ip+=data.p_stat;
		iT+=data.temp_out;

		if (init_cnt){
			omx_offset+=M_PI/180.0*3.0518e-7*(double)data.x_gyro/INIT_CNT;
			omy_offset+=M_PI/180.0*3.0518e-7*(double)data.y_gyro/INIT_CNT;
			omz_offset+=M_PI/180.0*3.0518e-7*(double)data.z_gyro/INIT_CNT;

			init_cnt--;
		} else {

			for(int i=0;i<3;i++){
				angle_last[i]=angle[i];
			}

			omx=M_PI/180.0*3.0518e-7*(double)data.x_gyro-omx_offset;
			omy=M_PI/180.0*3.0518e-7*(double)data.y_gyro-omy_offset;
			omz=M_PI/180.0*3.0518e-7*(double)data.z_gyro-omz_offset;



			angle_dot[0]   = omx  + tan( angle_last[1] ) * ( omy  * sin( angle_last[0] ) + omz  * cos( angle_last[0] ));
			angle_dot[1]   = omy  * cos( angle_last[0] ) - omz  * sin( angle_last[0] );
			angle_dot[2]   = 1.0/cos( angle_last[1] ) * ( omy  * sin( angle_last[0] ) + omz  * cos( angle_last[0] ));

			for(int i=0;i<3;i++){
				angle[i] = angle_last[i] + angle_dot[i] * (double)delta_ns/1e9; //delta_ns about 404e-6 s;
			}


		}

		sample_cnt++;

		if(sample_cnt==SAMPLE_LENGTH){
			sample_cnt=0;

			iax/=SAMPLE_LENGTH;
			iay/=SAMPLE_LENGTH;
			iaz/=SAMPLE_LENGTH;
			iomx/=SAMPLE_LENGTH;
			iomy/=SAMPLE_LENGTH;
			iomz/=SAMPLE_LENGTH;
			imagx/=SAMPLE_LENGTH;
			imagy/=SAMPLE_LENGTH;
			imagz/=SAMPLE_LENGTH;
			ip/=SAMPLE_LENGTH;

			iT/=SAMPLE_LENGTH;


			printf("delta t: %d ns\n",delta_ns);
			printf("  ax: %8.4lf   ay: %8.4lf   az: %8.4lf m/s^2\n", 1.2207e-7*(double)iax, 1.2207e-7*(double)iay, 1.2207e-7*(double)iaz);
			printf(" omx: %8.4lf  omy: %8.4lf  omz: %8.4lf deg/s\n", 3.0518e-7*(double)iomx-omx_offset*180.0/M_PI,3.0518e-7*(double)iomy-omy_offset*180.0/M_PI,3.0518e-7*(double)iomz-omz_offset*180.0/M_PI);
			printf("magx: %8.3lf magy: %8.3lf magz: %8.3lf uT\n", .01*(double)imagx,0.01*imagy,0.01*imagz);
			printf("p: %8.4lf hPa\n",6.1035e-7*(double)ip);
			printf("T: %5.3lf degC\n", 25.0+0.00565*(double)iT);
			printf("\n");

			if(init_cnt){
				printf("initialization, do not move\n\n");
			}

			phi   = 180.0/M_PI*angle[0];  //IMU up side down
			theta = -180.0/M_PI*angle[1]; //IMU up side down
			psi   = -180.0/M_PI*angle[2]; //IMU up side down

			printf("  phi: %8.4lf deg\n",phi);
			printf("theta: %8.4lf deg\n",theta);
			printf("  psi: %8.4lf deg\n",psi);
			printf("\n");


			iax=0;
			iay=0;
			iaz=0;

			iomx=iomy=iomz=0;
			imagx=imagy=imagz=0;
			ip=0;
			iT=0;


		}

	}

	close(fd);


	return 0;
}
