/****************************************************************************
 * examples/sensor/lib/uavcan/datatype/Data.cxx
 *
 *   Copyright (C) 2017 Jonas Mikolajczyk. All rights reserved.
 *   Author: Jonas Mikolajczyk <j.mikolajczyk@tu-braunschweig.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include "../../../../sensor/lib/uavcan/datatype/Data.h"

namespace uavcan {

/**
 Allocates the maximum needed size, of the uavcan datatype and initialize it with zeros.

 @param maxSize maximal number of bytes, the data needs.
 @return an uavcan data object.

 */

Data::Data()
  {
    size = NULL;
    data = NULL;
    signature = NULL;
    service = NULL;
    dataTypeId = NULL;
  }

Data::Data(uint8_t _size, uint64_t _signature, bool _service,
    uint16_t _dataTypeId)
  {
    size = _size;
    data = (uint8_t *) calloc(_size, 1);
    signature = _signature;
    service = _service;
    dataTypeId = _dataTypeId;
  }

uint64_t Data::getSignature()
  {
    return signature;
  }

bool Data::isService()
  {
    return service;
  }

int Data::setData(void* addData, uint16_t startBit, uint16_t bits)
  {
    if ((startBit + bits) > size * 8)
      {
        return -1;
      }
    uint8_t dataPos = (startBit) / 8;
    uint8_t addPos = 0;
    uint8_t offset = startBit % 8;

    uint8_t* add = (uint8_t*) addData;

    for (int pos = 0; pos < bits; pos += 8, dataPos++, addPos++)
      {
        uint8_t dataByte = add[addPos];
        uint8_t rest = bits - pos;

        if (bits - pos < 8)
          {
            dataByte = dataByte << (8 - rest);
            if (offset + rest > 8)
              {
                data[dataPos] |= (dataByte >> offset);
                data[dataPos + 1] |= (dataByte << (8 - offset));
              }
          }
        else
          {
            data[dataPos + 1] |= (dataByte << (8 - offset));
          }
        data[dataPos] |= (dataByte >> offset);
      }
    return 0;
  }

uint8_t Data::getSize()
  {
    return size;
  }

uint8_t * Data::getData()
  {
    return data;
  }

uint16_t Data::getDatatypeId()
  {
    return dataTypeId;
  }


uint16_t Data::getFloat16(float value)
  {
    /*
     * https://gist.github.com/rygorous/2156668
     * Public domain, by Fabian "ryg" Giesen
     */
    const Fp32 f32infty =
      { 255U << 23 };
    const Fp32 f16infty =
      { 31U << 23 };
    const Fp32 magic =
      { 15U << 23 };
    const uint32_t sign_mask = 0x80000000U;
    const uint32_t round_mask = ~0xFFFU;

    Fp32 in;
    uint16_t out;

    in.f = value;

    uint32_t sign = in.u & sign_mask;
    in.u ^= sign;

    if (in.u >= f32infty.u) /* Inf or NaN (all exponent bits set) */
      {
        /* NaN->sNaN and Inf->Inf */
        out = (in.u > f32infty.u) ? 0x7FFFU : 0x7C00U;
      }
    else /* (De)normalized number or zero */
      {
        in.u &= round_mask;
        in.f *= magic.f;
        in.u -= round_mask;
        if (in.u > f16infty.u)
          {
            in.u = f16infty.u; /* Clamp to signed infinity if overflowed */
          }

        out = uint16_t(in.u >> 13); /* Take the bits! */
      }

    out = uint16_t(out | (sign >> 16));

    return out;
  }

float Data::getFloat32(uint16_t value)
  {
    /*
     * https://gist.github.com/rygorous/2144712
     * Public domain, by Fabian "ryg" Giesen
     */
    const Fp32 magic =
      { (254U - 15U) << 23 };
    const Fp32 was_infnan =
      { (127U + 16U) << 23 };
    Fp32 out;

    out.u = (value & 0x7FFFU) << 13; /* exponent/mantissa bits */
    out.f *= magic.f; /* exponent adjust */
    if (out.f >= was_infnan.f) /* make sure Inf/NaN survive */
      {
        out.u |= 255U << 23;
      }
    out.u |= (value & 0x8000U) << 16; /* sign bit */

    return out.f;
  }

void Data::printData()
  {
    for (int i = 0; i < size; i++)
      {
        printf("Data[%i] = %u  |  0x%02X\n", i, data[i], data[i]);
      }
  }

Data::~Data()
  {
    if (data != NULL)
      {
        free(data);
        data = NULL;
      }
  }

} /* namespace uavcan */
