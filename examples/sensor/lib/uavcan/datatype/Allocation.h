/****************************************************************************
 * examples/sensor/lib/uavcan/datatype/Allocation.h
 *
 *   Copyright (C) 2017 Jonas Mikolajczyk. All rights reserved.
 *   Author: Jonas Mikolajczyk <j.mikolajczyk@tu-braunschweig.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#ifndef APPS_EXAMPLES_SENSOR_LIB_UAVCAN_DATATYPE_ALLOCATION_H_
#define APPS_EXAMPLES_SENSOR_LIB_UAVCAN_DATATYPE_ALLOCATION_H_

#include "../../../../sensor/lib/uavcan/datatype/Data.h"

namespace uavcan {

struct allocation_s {
  uint8_t node_id;
  bool first_part_of_unique_id;
  uint8_t *unique_id;
  uint8_t unique_id_size;
};

class Allocation: public Data {
  static const uint16_t DATATYPEID = 1;
  static const uint64_t SIGNATURE = 0xB2A812620A11D40UL;
  static const bool SERVICE = false;
  static const uint16_t MAX_REQUEST_PERIOD_MS = 1000;
  static const uint16_t MIN_REQUEST_PERIOD_MS = 600;
  static const uint16_t MAX_FOLLOWUP_DELAY_MS = 400;
  static const uint16_t MIN_FOLLOWUP_DELAY_MS = 0;
  static const uint16_t FOLLOWUP_TIMEOUT_MS = 500;
  static const uint8_t MAX_LENGTH_OF_UNIQUE_ID_IN_REQUEST = 6;

  void setNodeId(uint8_t id);
  void setFirstPart(bool isFirstPart);
  void setUniqueId(uint8_t* unique_id, uint8_t idSize);

public:
  static uint16_t getDatatype()
    {
      return DATATYPEID;
    }
  Allocation(allocation_s &alloc_data);
  virtual ~Allocation();
};

} /* namespace uavcan */

#endif /* APPS_EXAMPLES_SENSOR_LIB_UAVCAN_DATATYPE_ALLOCATION_H_ */
