/****************************************************************************
 * examples/sensor/lib/uavcan/node/Node.cxx
 *
 *   Copyright (C) 2017 Jonas Mikolajczyk. All rights reserved.
 *   Author: Jonas Mikolajczyk <j.mikolajczyk@tu-braunschweig.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include "../../../../sensor/lib/uavcan/node/Node.h"
#include <time.h>

namespace uavcan {
/*
 * Creates an Node object and starts the dynamic id allocation.
 */
Node* Node::nodeInstance = 0;
uint8_t Node::UNIQUE_ID[] =
  { 0x49, 0x46, 0x46, 0x2e, 0x53, 0x69, 0x6d, 0x75, 0x6c, 0x61, 0x6c, 0x61,
      0x74, 0x69, 0x6f, 0x6e };

Node::Node()
  {
    savedNodeId = 0;
    //posCurrentUniqueId = 0;
    last_message_timestamp = 0;
    nodeId = 0;
    msgList = NULL;
    isStarted = false;
    isInsideAllocation = false;
    allocated = false;
    allocationSendState = 0;
    allocationReceiveState = 0;
    CanDriver::getDriver()->open_uavcan();
  }

Node* Node::getNode()
  {
    if (nodeInstance == 0)
      {
        nodeInstance = new Node();
      }
    return nodeInstance;
  }

/*
 * If the node id is not already allocated, which will happen, when the node starts, than this methods allow to set an id and
 * use a static node id.
 */
int Node::setNodeId(uint8_t id) {
	if (id > 0 && id <= 127) {
		nodeId = id;
		allocated = true;
		return 0;
	}
	return -1;
}

/*
 * Sets the priority of a specific datatype specified by its datatype id. If this datatype will be send via uavcan, the priority
 * will be set in the first 5 bits of the id of the can message.
 */
void Node::setPriority(uint16_t dataId, uint8_t prio)
  {
	if (prio > 63) {
		addInfo(dataId, 63);
	} else {
		addInfo(dataId, prio);
	}
  }



/*
 * Returns a pointer to the linked list item, which contains the the transfer information for the specific
 * datatype.
 */
Node::node_s* Node::getInfo(uint16_t datatypeId)
  {
    node_s * nodePtr = msgList;
    while (nodePtr != NULL)
      {
        if (datatypeId == nodePtr->msg_transfer.dataTypeId)
          {
            return nodePtr;
          }
        nodePtr = nodePtr->next;
      }
    return NULL;
  }

/*
 * Adds a new entry to the linked list with the information of the datatype id and the priority. If the datatypes already exist, it overrides the information.
 */
Node::node_s* Node::addInfo(uint16_t datatypeId, uint8_t prio)
  {
    node_s * nodePtr = msgList;
    node_s * newNode;

    if (nodePtr != NULL)
      {
        /*
         * Iterate over the linked list, until an entry is equal to the datatypeId or
         * the end of the list is reached.
         */
        while (nodePtr->next != NULL
            && nodePtr->msg_transfer.dataTypeId != datatypeId)
          {
            nodePtr = nodePtr->next;
          }
        // Entry already exist.
        if (nodePtr->msg_transfer.dataTypeId == datatypeId)
          {
            newNode = nodePtr;
          }
        else
          {
            // Entry does not already exist. Creates a new container object.
            nodePtr->next = new node_s;
            newNode = nodePtr->next;
          }
      }
    else
      {
        msgList = new node_s;
        newNode = msgList;
      }

    /*
     * Sets the information of the datatype. The transferId is not set here, it gets
     * set by creation of the object to 0.
     */
    newNode->msg_transfer.prio = prio;
    newNode->msg_transfer.dataTypeId = datatypeId;

    return newNode;
  }

int Node::start()
  {


//    int ret = CanDriver::getDriver()->open_uavcan();
//    if (ret < 0)
//      {
//        return ret;
//      }
//    allocation_s allocMsg;
//	allocMsg.first_part_of_unique_id = true;
//	allocMsg.node_id = nodeId;
//	allocMsg.unique_id = UNIQUE_ID;
//	allocMsg.unique_id_size = 6;
//    isInsideAllocation = true;
//
//    while (true) {
//		allocMsg.unique_id_size = 6;
//		allocMsg.unique_id = UNIQUE_ID;
//		Allocation allocationMsg1 = Allocation(allocMsg);
//    		send(&allocationMsg1);
//    }
    if (isStarted && allocated && nodeId > 0)
      {
        return 0;
      }
    else
      {
    	if (allocated && nodeId > 0) {
    		isStarted = true;
    		return 0;
    	}
        allocateId();
        if (nodeId > 0 && allocated)
          {
            isStarted = true;
            return 0;
          }
        else
          {
            return -1;
          }
      }
  }

void Node::printInfo()
  {
    node_s* ptr = msgList;
    while (ptr != NULL)
      {
        printf("Datatype: %04i  TransferID: %02i  Prio: %02i\n",
            ptr->msg_transfer.dataTypeId, ptr->msg_transfer.transferId,
            ptr->msg_transfer.prio);
        ptr = ptr->next;
      }
  }

void Node::stop()
  {
    CanDriver::getDriver()->close_uavcan();
    delete nodeInstance;
    nodeInstance = NULL;
  }

int Node::restart()
  {
    CanDriver::getDriver()->restart_uavcan();
    int ret;
    ret = start();
    return ret;
  }

Node::~Node()
  {
    printf("Deleting Node\n");
    node_s *current = msgList;
    node_s *next;

    while (current != NULL)
      {
        next = current->next;
        free(current);
        current = next;
      }
    msgList = NULL;
  }

//bool Node::isFirstPart(uint8_t *unique_id)
//  {
//    bool isFirst = true;
//    for (int i = 0; i < 6; i++)
//      {
//        isFirst = isFirst && (unique_id[i] == UNIQUE_ID[i + 1]);
//      }
//    return isFirst;
//  }

//int Node::uniqueSize(uavcan_msg_s *msg)
//  {
//    if (msg->isMultiframe() && msg->isStart())
//      {
//        return msg->payload->size - 3;
//      }
//    else
//      {
//        if (msg->isStart())
//          {
//            return msg->payload->size - 1;
//          }
//      }
//    return msg->payload->size;
//  }

// This is an internal function; see below.
//int Node::detectRequestStage(uavcan_msg_s *msg)
//  {
//    if (uniqueSize(msg) == 6 && isFirstPart(msg->payload->data + 1))
//      {
//        return 1;
//      }
//    if (uniqueSize(msg) == 7)
//      {
//        return 2;
//      }
//    if (uniqueSize(msg) < 6)
//      {
//        return 3;
//      }
//    return 0;
//  }

// This is an internal function; see below.
//int Node::getExpectedStage()
//  {
//    if (posCurrentUniqueId == 0)
//      {
//        return 1;
//      }
//    if (posCurrentUniqueId >= 12)
//      {
//        return 3;
//      }
//    if (posCurrentUniqueId >= 6)
//      {
//        return 2;
//      }
//    return 0;
//  }

//int Node::getUniqueIdTransferSize()
//  {
//    int stage = getExpectedStage();
//    if (stage == 0)
//      {
//        return 6;
//      }
//    if (stage == 1)
//      {
//        return 6;
//      }
//    if (stage == 2)
//      {
//        return 4;
//      }
//    return 0;
//  }

int Node::getUniqueIdTransferSize() {
	if (allocationSendState < 2) {
		return 6;
	}
	if (allocationSendState == 2) {
			return 4;
	}
	return 0;
}

int Node::getUniqueIdTransferPosition() {
	if (allocationSendState == 0) {
		return 0;
	}
	if (allocationSendState == 1) {
			return 6;
	}
	if (allocationSendState == 2) {
			return 12;
	}
	return 0;
}


void Node::sendAllocationMsg() {
    allocation_s allocMsg;

    allocMsg.first_part_of_unique_id = allocationSendState == 0 ? true :  false;
    allocMsg.node_id = nodeId;
    allocMsg.unique_id = UNIQUE_ID + getUniqueIdTransferPosition();
    allocMsg.unique_id_size = getUniqueIdTransferSize();

    Allocation allocationMsg = Allocation(allocMsg);
    send(&allocationMsg);
    timespec time;
    clock_gettime(CLOCK_REALTIME, &time);
    last_message_timestamp = (time.tv_sec*1000) + (time.tv_nsec/1000000);
    allocationSendState++;
}


void Node::allocateId()
  {
    if (!allocated)
      {
        CanDriver::getDriver()->setCallbackFunction(Allocation::getDatatype(),
            &handleAllocation);
        isInsideAllocation = true;

    	printf("Start Allocation\n");
    	fflush(stdout);

        timespec time;
        bool first = true;


        while (!allocated)
          {
            clock_gettime(CLOCK_REALTIME, &time);
            printf("Last: %i  Current: %i  Diff: %i  \n",
            		last_message_timestamp, time.tv_sec*1000 + time.tv_nsec/1000000, time.tv_sec*1000 + time.tv_nsec/1000000 - last_message_timestamp);
            fflush(stdout);
        	if (!first && (time.tv_sec*1000 + time.tv_nsec/1000000) - last_message_timestamp > 3000) {
        		printf("Restart Allocation\n");
        		fflush(stdout);
        		allocationReceiveState = 0;
        		allocationSendState = 0;
        		savedNodeId = 0;
        		sendAllocationMsg();
        	}
        		/*
        		 * Read the data of the driver every 50 ms as long, as we are not allocated.
        		 * The read uavcan message will callback the handle
        		 * allocation method, which will take care of setting the node id.
        		 */
        	    CanDriver::getDriver()->read_uavcan();
        	    if (first) {
        	    	usleep(3*1000*1000);
        	    	first = false;
        	    	sendAllocationMsg();
        	    } else {
        	    	usleep(200000);
        	    }
          }
        CanDriver::getDriver()->removeCallbackFunction(Allocation::getDatatype());
        printf("Finished Allocation. Id = %i\n", nodeId);
      }
  }

bool Node::isPartOfUniqueId(uint8_t *id, uint8_t size) {
	uint8_t j = 0;
	uint8_t count = 0;
	for (uint8_t i = 0; i < 16 && j < size; i++) {
		if (UNIQUE_ID[i] == id[j]) {
			j++;
			count++;
		}
	}
	if (count == size) return true;
	return false;
}


void Node::handleAllocation(uavcan_msg_s *msg)
  {
	printf("ReceiveState %i   SendState %i\n", nodeInstance->allocationReceiveState, nodeInstance->allocationSendState);
	// If no answer is received in the last 1.5 seconds, restart the id allocation
	if((msg->timestamp.tv_sec*1000 + msg->timestamp.tv_nsec/1000000) - nodeInstance->last_message_timestamp > 1500) {
		printf("Send Message Time: %i ms\n", nodeInstance->last_message_timestamp);
		printf("Received Message Time: %i ms\n", (msg->timestamp.tv_sec*1000 + msg->timestamp.tv_nsec/1000000));
		printf("Delta Time: %i ms\n", (msg->timestamp.tv_sec*1000 + msg->timestamp.tv_nsec/1000000)-nodeInstance->last_message_timestamp);
		nodeInstance->allocationReceiveState = 0;
		nodeInstance->allocationSendState = 0;
		nodeInstance->savedNodeId = 0;
		nodeInstance->sendAllocationMsg();
		return;
	}
	// If the node is already allocated, ignore this message
	if (nodeInstance->allocated) {
		return;
	}
	// If the message is anonymous, ignore this message
	if (msg->isAnonymousTransfer()) {
		return;
	}
	// If its a multiframe message and first frame and the node_id field is != 0, save the id
	if (msg->isStart() && msg->isMultiframe() && (msg->payload[0].data[2] & 0xfe) != 0) {
		nodeInstance->savedNodeId = (msg->payload[0].data[2] & 0xfe) >> 1;
	}
	// If the message is the last frame, and contains data of the Unique Id,  increase the allocationReceiveState
	if (msg->isEnd()) {
		if (msg->isStart()) {
			if ( isPartOfUniqueId(msg->payload[0].data+1, msg->payload->size-1) ) {
				nodeInstance->allocationReceiveState++;
			}
		} else {
			if ( isPartOfUniqueId(msg->payload[0].data, msg->payload->size) ) {
				nodeInstance->allocationReceiveState++;
			}
		}
	} else {
		return;
	}
	// If the receive state is 3, so the last respone is send, set the node Id and set the state to allocated
	if (nodeInstance->allocationReceiveState == 3) {
		nodeInstance->nodeId = nodeInstance->savedNodeId;
		nodeInstance->allocated = true;
		nodeInstance->isInsideAllocation = false;
		return;
	}
	// If the receiveState increased, but the allocation is not finished, send the follow up message
	if (nodeInstance->allocationReceiveState == nodeInstance->allocationSendState) {
		nodeInstance->sendAllocationMsg();
	}
  }









// This function is invoked when the allocator receives a message of type uavcan.protocol.dynamic_node_id.Allocation.
//void Node::handleAllocation(uavcan_msg_s *msg)
//  {
//    if (!msg->isAnonymousTransfer())
//      {
//        return;         // This is a response from another allocator, ignore
//      }
//
//    // Reset the expected stage on timeout
//    if (msg->getMonotonicTimestampMs()
//        > (nodeInstance->last_message_timestamp + 500))
//      {
//        nodeInstance->posCurrentUniqueId = 0;
//      }
//
//    if (msg->isMultiframe() && msg->isStart()
//        && (msg->payload->data[2] & 0xfe) != 0x00)
//      {
//        nodeInstance->savedNodeId = (msg->payload->data[0] & 0xfe) > 1;
//      }
//
//    // Checking if request stage matches the expected stage
//    int request_stage = nodeInstance->detectRequestStage(msg);
//    if (request_stage == 0)
//      {
//        return;             // Malformed request - ignore without resetting
//      }
//
//    if (request_stage != nodeInstance->getExpectedStage())
//      {
//        return;             // Ignore - stage mismatch
//      }
//
//    int msgUniqueSize = nodeInstance->uniqueSize(msg);
//    if (msgUniqueSize > 16 - nodeInstance->posCurrentUniqueId)
//      {
//        return;             // Malformed request
//      }
//
//    // Updating the local state
//    nodeInstance->posCurrentUniqueId += msgUniqueSize;
//
//    if (nodeInstance->posCurrentUniqueId == 16)
//      {
//        nodeInstance->nodeId = nodeInstance->savedNodeId;
//        nodeInstance->posCurrentUniqueId = 0;
//        nodeInstance->isInsideAllocation = false;
//        nodeInstance->allocated = true;
//        CanDriver::getDriver()->removeCallbackFunction(
//            Allocation::getDatatype());
//      }
//    else
//      {
//        // Publishing the follow-up if possible.
//        allocation_s allocMsg;
//        allocMsg.first_part_of_unique_id = (nodeInstance->posCurrentUniqueId
//            == 0);
//        allocMsg.node_id = nodeInstance->nodeId;
//        allocMsg.unique_id_size = nodeInstance->getUniqueIdTransferSize();
//        allocMsg.unique_id = new uint8_t[allocMsg.unique_id_size];
//        memcpy((void*) allocMsg.unique_id, (void*) UNIQUE_ID,
//            allocMsg.unique_id_size);
//
//        Allocation broadCastMsg = Allocation(allocMsg);
//        nodeInstance->send(&broadCastMsg);
//      }
//
//    // It is important to update the timestamp only if the request has been processed successfully.
//    nodeInstance->last_message_timestamp = msg->getMonotonicTimestampMs();
//  }

} /* namespace uavcan */
