/****************************************************************************
 * examples/sensor/lib/uavcan/node/Node.h
 *
 *   Copyright (C) 2017 Jonas Mikolajczyk. All rights reserved.
 *   Author: Jonas Mikolajczyk <j.mikolajczyk@tu-braunschweig.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#ifndef APPS_EXAMPLES_SENSOR_LIB_UAVCAN_NODE_NODE_H_
#define APPS_EXAMPLES_SENSOR_LIB_UAVCAN_NODE_NODE_H_

#include "../../../../sensor/lib/driver/CanDriver.h"
#include "../../../../sensor/lib/uavcan/datatype/Allocation.h"
#include "../../../../sensor/lib/uavcan/message/Message.h"

namespace uavcan {

class Node {
  static Node *nodeInstance;
  //Contains the transferId and Priority for each send datatype
  struct msg_transfer_s {
    msg_transfer_s()
        : dataTypeId(0), transferId(0), prio(10)
      {
      }
    uint16_t dataTypeId;
    uint8_t transferId;
    uint8_t prio;

  };

  // Linked list to dynamic create new msg_transfer_s containers
  struct node_s {
    node_s()
        : next(NULL)
      {
      }
    msg_transfer_s msg_transfer;
    node_s *next;
  };

  uint8_t nodeId;
  node_s *msgList;

  node_s* getInfo(uint16_t id);
  node_s* addInfo(uint16_t datatypeId, uint8_t prio);

  static uint8_t UNIQUE_ID[];

  // State variables - Id Allocation:
  uint32_t last_message_timestamp;
  //uint8_t posCurrentUniqueId;
  uint8_t allocationSendState;
  uint8_t allocationReceiveState;
  uint8_t savedNodeId;


  //Status variable
  bool isStarted;
  bool isInsideAllocation;
  bool allocated;

  Node();
  bool isFirstPart(uint8_t *unique_id);
  int uniqueSize(uavcan_msg_s *msg);
  int detectRequestStage(uavcan_msg_s *msg);
  int getExpectedStage();

  int getUniqueIdTransferSize();
  void allocateId();

  void sendAllocationMsg();
  int getUniqueIdTransferPosition();
  static bool isPartOfUniqueId(uint8_t *id, uint8_t size);

public:
  static void handleAllocation(uavcan_msg_s *msg);
  static Node* getNode();
  int setNodeId(uint8_t id);
  int start();
  void stop();
  int restart();
  void printInfo();

  /*
   * Looks for the data information in the linked list. If the data information does not exist, it
   * creates a new entry. After that it creates an Message Object and invokes send on the object.
   * After that it increases the transferId of the datatype.
   */
  template<class T>
  int send(T *data)
    {
      if ((isStarted && allocated) || isInsideAllocation)
        {
		  /*
		   * Don´t allow to send messaged despite allocation messages during the id allocation process.
		   */
		  if (isInsideAllocation && data->getDatatypeId() != Allocation::getDatatype())
			{
			  return -1;
			}
          node_s* info = getInfo(data->getDatatypeId());
          if (info == NULL)
            {
              info = addInfo(data->getDatatypeId(), 10);
            }
          Message msg = Message(data, info->msg_transfer.transferId,
              info->msg_transfer.prio, nodeId);

          msg.send();
          if (data->getDatatypeId() != Allocation::getDatatype()) {
        	  //CanDriver::getDriver()->restart_uavcan();
          }
          info->msg_transfer.transferId += 1;
          return 0;
        }
      else
        {
          return -1;
        }
    }

  /*
   * Sets the priority of a specific datatype. If the entry does not exist, a new entry gets created.
   */
  void setPriority(uint16_t dataId, uint8_t prio);
  virtual ~Node();
};

} /* namespace uavcan */

#endif /* APPS_EXAMPLES_SENSOR_LIB_UAVCAN_NODE_NODE_H_ */
