#
# For a description of the syntax of this configuration file,
# see the file kconfig-language.txt in the NuttX tools repository.
#

menuconfig EXAMPLES_SENSOR
	bool "Sensor example"
	default n
	---help---
		Enable the Sensor example

if EXAMPLES_SENSOR

config EXAMPLES_SENSOR_PROGNAME
	string "Program name"
	default "sensor"
	depends on BUILD_KERNEL
	---help---
		This is the name of the program that will be use when the NSH ELF
		program is installed.

		
config EXAMPLES_SENSOR_STACKSIZE
	int "Application stack size"
	default 2048

config EXAMPLES_SENSOR_TASK_PRIORITY
	int "Application priority"
	default 12
	
comment "Pressure/Temperature Sensor"

config EXAMPLES_SENSOR_PRESSURE_0
	int "Pressure in Pa at 0 m"
	default 101325
	---help---
		The pressure is used for the calculation of the pressure in a given heigt.
		
config EXAMPLES_SENSOR_PRESSURE_VARIANCE
	int "Pressure variance in Pascal^2 * e-2"
	default 1
	---help---
		Sets the pressure variance in Pascal^2 * e-2. (Setting the value to 1 means 0.01 Pa^2)
	
config EXAMPLES_SENSOR_TEMPERATURE_0
	int "Temperature in degree C at 0 m"
	default 20
	---help---
		The temperaure is used for the calculation of the pressure in a given heigt.

config EXAMPLES_SENSOR_TEMPERATURE_VARIANCE
	int "Temperature variance in Kelvin^2 * e-2"
	default 1
	---help---
		Sets temperature variance in Kelvin^2 * e-2. (Setting the value to 1 means 0.01 K^2).

comment "Magnetometer"

config EXAMPLES_SENSOR_MAGNET_DECLINATION
	int "Declination in degrees"
	default 3
	range -90 90
	---help---
		Sets the inclination in degrees. Values between -90 and 90 are valid.


config EXAMPLES_SENSOR_MAGNET_INCLINATON
	int "Inclination in degrees"
	default 68
	range -90 90
	---help---
		Sets the declination in degrees. Values between -90 and 90 are valid.

config EXAMPLES_SENSOR_MAGNET_VARIANCE
	int "Magnetometer variance in Gauss^2 * e-2"
	default 1
	---help---
		Sets the magnetic variance in gauss^2 * e-2. (Setting the value to 1 means 0.01 Gauss^2)



comment "GPS/GNSS"

config EXAMPLES_SENSOR_GNSS_LEAPSECONDS
	int "Leap seconds"
	default 27
	---help---
		Sets the leap seconds.

config EXAMPLES_SENSOR_GNSS_HDOP
	int "HDOP (horizontal dilution of precision * e-2"
	default 1
	---help---
		Sets the HDOP (horizontal dilution of precision). (Setting the value to 1 means an HDOP of 0.01)


config EXAMPLES_SENSOR_GNSS_VDOP
	int "VDOP (vertical dilution of precision) * e-2"
	default 1
	---help---
		Sets the VDOP (vertical dilution of precision).  (Setting the value to 1 means an VDOP of 0.01)

config EXAMPLES_SENSOR_GNSS_TDOP
	int "TDOP (time dilution of precision) * e-2"
	default 1
	---help---
		Sets the TDOP (time dilution of precision).  (Setting the value to 1 means an TDOP of 0.01)

config EXAMPLES_SENSOR_GNSS_SATELITES
	int "Number of satelites"
	default 20
	---help---
		Sets the number of satelites used.


config EXAMPLES_SENSOR_GNSS_POSITION_VARIANCE
	int "Position variance in meter^2 * e-3"
	default 1
	---help---
		Sets the position variance in meter^2 * e-3. (Setting the value to 1 means 0.001 m^2)


config EXAMPLES_SENSOR_GNSS_VELOCITY_VARIANCE
	int "Velocity variance in meter per second^2 * e-3"
	default 1
	---help---
		Sets the velocity variance in m/s^2 * e-3. (Setting the value to 1 means 0.001 (m/s)^2)


endif

