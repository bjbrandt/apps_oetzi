############################################################################
# apps/examples/sensor/Makefile
#
#   Copyright (C) 2017 Jonas Mikolajczyk. All rights reserved.
#   Author: Jonas Mikolajczyk <j.mikolajczyk@tu-braunschweig.de>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in
#    the documentation and/or other materials provided with the
#    distribution.
# 3. Neither the name NuttX nor the names of its contributors may be
#    used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
# COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
# OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
# AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
############################################################################

-include $(TOPDIR)/Make.defs

CONFIG_EXAMPLES_SENSOR_PRIORITY ?= SCHED_PRIORITY_DEFAULT
CONFIG_EXAMPLES_SENSOR_STACKSIZE ?= 2048


APPNAME = sensor
UAVCAN_DIR = lib/uavcan
DRIVER_DIR = lib/driver
SENSOR_DIR = lib/sensor
CMATH_DIR = lib/math_custom
DATA_DIR = $(UAVCAN_DIR)/datatype
MESSAGE_DIR = $(UAVCAN_DIR)/message
NODE_DIR = $(UAVCAN_DIR)/node


PRIORITY = $(CONFIG_EXAMPLES_SENSOR_PRIORITY)
STACKSIZE = $(CONFIG_EXAMPLES_SENSOR_STACKSIZE)

# Sensor Example

ASRCS =
CSRCS += $(CMATH_DIR)/powf_custom.c
CSRCS += $(CMATH_DIR)/logf_custom.c
MAINSRC = sensor_main.cxx

# Datatypes
CXXSRCS += $(DATA_DIR)/Data.cxx
CXXSRCS += $(DATA_DIR)/MagneticFieldStrength.cxx
CXXSRCS += $(DATA_DIR)/StaticPressure.cxx
CXXSRCS += $(DATA_DIR)/StaticTemperature.cxx
CXXSRCS += $(DATA_DIR)/Auxiliary.cxx
CXXSRCS += $(DATA_DIR)/Allocation.cxx
CXXSRCS += $(DATA_DIR)/Fix.cxx
CXXSRCS += $(DATA_DIR)/NodeStatus.cxx
CXXSRCS += $(SENSOR_DIR)/magnet.cxx

# Message and Node
CXXSRCS += $(MESSAGE_DIR)/Message.cxx
CXXSRCS += $(NODE_DIR)/Node.cxx

# Driver
CXXSRCS += $(DRIVER_DIR)/CanDriver.cxx


CONFIG_EXAMPLES_SENSOR_PROGNAME ?= sensor$(EXEEXT)
PROGNAME = $(CONFIG_EXAMPLES_SENSOR_PROGNAME)

clean:: clean_sensor

clean_sensor::
	$(Q) rm -f $(DRIVER_DIR)/CanDriver.o
	$(Q) rm -f $(DATA_DIR)/Data.o
	$(Q) rm -f $(DATA_DIR)/MagneticFieldStrength.o
	$(Q) rm -f $(DATA_DIR)/StaticPressure.o
	$(Q) rm -f $(DATA_DIR)/StaticTemperature.o
	$(Q) rm -f $(DATA_DIR)/Auxiliary.o
	$(Q) rm -f $(DATA_DIR)/Allocation.o
	$(Q) rm -f $(DATA_DIR)/Fix.o
	$(Q) rm -f $(DATA_DIR)/NodeStatus.o
	$(Q) rm -f $(SENSOR_DIR)/magnet.o
	$(Q) rm -f $(MESSAGE_DIR)/Message.o
	$(Q) rm -f $(NODE_DIR)/Node.o

include $(APPDIR)/Application.mk
