# Emulation von GNSS-, Magnetometer-, Drucksensor- und Temperatursensor-Daten

Diese App erzeugt **Stützinformationen** für den Open Source Autopiloten ArduPilot. Diese Informationen werden über den CAN-Bus mittels des von ArduPilot unterstützten Protokolls [UAVCAN](http://uavcan.org) übermittelt. 

## Inhaltsverzeichnis
[TOC]

## Installation der App
Um die App verwenden zu können, müssen am NuttX-Betriebssystem und an der ArduPilot-Firmware bestimmte Einstellungen vorgenommen werden.
### Einstellungen NuttX
Bevor NuttX kompiliert wird, lässt sich mittels des Befehls

> make menuconfig

einstellen, welche Treiber, Bibliotheken und Apps mit kompiliert werden sollen. Damit die App Daten über den CAN-Bus senden kann, muss der CAN-Bus sowie der Treiber aktiviert werden. Da das UAVCAN-Protokoll auf CAN-Nachrichten mit *Extenden Id* aufbaut, muss dies beim CAN-Treiber explizit eingestellt werden. Die App ist in **C++** implemententiert, daher wird ein C++ Compiler benötigt. Zur Berechnung des Druckes und des Magnetfeld-Vektors wird die Mathe Bibliothek benönigt. Zudem muss die App selbst mit hinzugefügt werden. Weiterhin muss sichergestellt werden, dass eine Nachricht in der Message Queue 56 Byte groß sein darf, damit die Daten, welche fuer das Senden benoetigt werden, in eine Message Queue passen.

```
System Type
    STM32 Peripheral Support
        [*] CAN1
RTOS Features
    POSIX Message Queue Options
        (56) Maximum message size
Device Drivers
    Can Driver Support
        [*] Can extended IDs
Library Routines
    [*] Standard Math library
    [*] Have C++ compiler
Application Configuration
    Examples
        [*] Sensor example
```

Nachdem die App (Sensor example) aktiviert wurde, hat man Zugriff auf ein Untermenü, welches weitere Einstellungen bezüglich der über UAVCAN gesendeted Daten ermöglicht.

```
 --- Sensor example
 
*** Pressure/Temperature Sensor ***
(101325) Pressure in Pa at 0 m
(1)      Pressure variance in Pascal^2 * e-2
(20)     Temperature in degree C at 0 m 
(1)      Temperature variance in Kelvin^2 * e-2

*** Magnetometer ***
(3)      Declination in degrees
(68)     Inclination in degrees
(1)      Magnetometer variance in Gauss^2 * e-2

*** GPS/GNSS ***
(27)     Leap seconds
(100)    HDOP (horizontal dilution of precision * e-2
(100)    VDOP (vertical dilution of precision) * e-2
(1)      TDOP (time dilution of precision) * e-2
(20)     Number of satelites
(1)      Position variance in meter^2 * e-3
(1)      Velocity variance in meter per second^2 * e-3
```
#### Pressure/Temperature Sensor 
| Default | Einheit | Name | Erläuterung |
|---------|---------|------|-------------|
| 101325 | Pa | Pressure in Pa at 0 m | Druck in einer Höhe von 0 m |
| 1 | e-2*Pa^2 | Pressure variance in Pascal^2 * e-2 | Varianz des Drucks in hundertstel Pa^2
| 20 | °C |    Temperature in degree C at 0 m | Temperatur in einer Höhe von 0 m
| 1 | e-2*K^2 |      Temperature variance in Kelvin^2 * e-2 | Varianz der Temperatur in hundertstel K^2



#### Magnetometer
| Default | Einheit | Name | Erläuterung |
|---------|---------|------|-------------|
| 3 | ° | Declination in degrees | Deklination |
| 68 | ° | Inclination in degrees | Inklination
| 1 | e-2*Gauss^2 | Magnetometer variance in Gauss^2 * e-2 | Varianz der Magnetometer-Werte in hundertstel Gauss^2

#### GPS/GNSS
| Default | Einheit | Name | Erläuterung |
|---------|---------|------|-------------|
| 27    | s       |   Leap seconds                              | Schaltsekunden von UT1 zu UTC
| 100  | /*e-2       | HDOP (horizontal dilution of precision * e-2 | Horizontaler DOP - Wert wird in hunderdstel angegeben, um eine bessere Auflösung zu erzielen
| 100  | *e-2       | VDOP (vertical dilution of precision) * e-2  | Vertikaler DOP - Wert wird in hunderdstel angegeben, um eine bessere Auflösung zu erzielen
| 1    | *e-2      | TDOP (time dilution of precision) * e-2      | Zeit DOP - Wert wird in hunderdstel angegeben, um eine bessere Auflösung zu erzielen
| 20   | #       | Number of satelites                          | Anzahl der Sateliten
| 1    | e-3* * m^2     | Position variance in meter^2 * e-3                 | Varianz der Position in tausendstel meter^2
| 1    | e-3 * (m/s)^2 |  Velocity variance in meter per second^2 * e-3     | Varianz der Geschwindikeit in tausendstel (m/s)^2

> ACHTUNG: Die NuttX-Treiber-Implementierung von CAN legt beim Senden keine FIFO-Reihenfolge, sondern ein Prioritätsgesteuertes senden, fest. Dadurch kommt es beim Senden der CAN-Nachrichten zu einer falschen Reihenfolge, wodurch die Nachrichten von Ardupilot nicht mehr interpretiert werden können. Es ist sicher zustellen, dass der Treiber die Nachricht in FIFO-Reihenfolge sendet.

### Einstellungen ArduPilot
Damit ArduPilot die über CAN gesendeten Daten verarbeitet, muss eingestellt werden, dass Daten über den CAN-Bus gesendet werden. Weiterhin muss eingestellt werden, dass ArduPilot für den Druck, die Temperatur sowie den Magnetometer-Daten, die Daten eines externen Sensors denen der internen Sensoren bevorzugt. Diese Einstellungen können über [Mission Planner](https://github.com/ArduPilot/MissionPlanner#missionplanner) durchgeführt werden.

```
INITIAL SETUP
    Mandatory Hardware
        Compass
            Compass #1
                [x] Use this compass    
                [x] Externally mounted  //Externen Magnetometer wird verwendet
CONFIG/TUNING
    Full Parameter Tree
        BRD_CAN_ENABLE  2  // UAVCAN mit Dynamic Id Allocation wird aktiviert
        GND_PRIMARY     1  // Ardupilot verwendet externen Druck/Temperatur-Sensor
        GPS_TYPE        9  // GPS Daten werden über UAVCAN gelesen

```

> Hier sei am Rande erwähnt, dass nicht alle Versionen von ArduPilot (ArduCopter, ArduPlane, ArduSub, ArduRover) sowie deren Releases UAVCAN unterstützen. Dabei gibt die folgende Tabelle an, welche Releases der einzelnen ArduPilot-Versionen UAVCAN unterstützen. Getestet wurde es beim ArduRover. Dabei ergab sich, dass nur die Versionen 3.0.0 und 3.0.1 UAVCAN unterstützten. Neuere Versionen haben UAVCAN implementiert, es wird aber beim Start des Autopiloten nicht aktiviert. Dies lässt sich auch nicht über die Parameterliste einstellen.

|  Typ  |   Version  |
|-----------|-----------------|
| ArduCopter | 3.3.1 und neuer |
| ArduPlane | 3.2.1 und neuer |
| ArduRover | 3.0.0 und 3.0.1 |
| ArduSub | nicht unterstützt |


## Verwendung der App

Um Daten senden zu koennen, benoetigt die App die Informationen, zu den Daten, die gesendet werden sollen. Dazu sind folgende Informationenen noetig, die in folgenden **struct** verpackt werden muessen:

```C++
struct uavcanData_s{
    uint64_t timestamp;     // 8  Byte
    int32_t height_mm;      // 4  Byte
    int64_t longitude_1e8;  // 8  Byte
    int64_t latitude_1e8;   // 8  Byte
    float velocity[3];      // 12 Byte
    float yaw_degree;       // 4  Byte
    float pitch_degree;     // 4  Byte
    float roll_degree;      // 4  Byte
                            // -> 52 Byte
};
```

### Starten
Dieser **struct** wird zur Uebergabe an die App in eine *Message-Queue* geschrieben, aus der die App die Daten liest. Der Name der *Message-Queue* wird beim erstmaligen Aufrufen der App an die App übergeben. Beispiel zum starten der App aus einer anderen App:
```C++
#define QUEUE_NAME "/SensorQueue"
#define UAVCAN_START 0x00
#define UAVCAN_SEND 0x01
#define UAVCAN_STOP 0x02

void startApp() {
    struct mq_attr attr;
	attr.mq_curmsgs = 0;
	attr.mq_flags = 0;
	attr.mq_maxmsg = 4;
	attr.mq_msgsize = 32;
    mqd_t queue;									// Variable fuer die Queue erstellen, damit sie spaeter wieder geschlossen werden kann
	queue = mq_open(QUEUE_NAME, O_WRONLY | O_CREAT, 0664, &attr);
    sensor_main(UAVCAN_START, QUEUE_NAME);			// Aufruf der Sensor App mit uebergabe des Queue-Namen.
}
```
Beim erstmaligen Aufrufen der App initialisiert sich die App. Dabei werden folgende Dinge erledigt:

* Oeffnen der *Message-Queue*
* Initialisierung der Nachrichten *MagneticFieldStrength*, *Fix*, *Auxiliary*, *StaticPressure*, *StaticTemperature* und *NodeStatus* mit den vorgegebenen Standartwerten.
* Erstellen eines Tasks, welcher einen UAVCAN-Node erstellt. Dabei wird die Kommunikation zur Anfrage einer Id durchgefuehrt.

### Senden

Zum Senden der UAVCAN-Nachrichten ueber den CAN-Bus muessen zunaechst die zu sendenden Daten in die zuvor geoeffnete Queue geschrieben werden. Anschließend wird die App erneuert aufgeruft. Beispiel Implementierung:

```C++
void sendMessage() {
    // Erstellen des structs und speichern der Daten im struct
    struct uavcanData_s data;
    data.timestamp = 123983993024;
    data.heigt_mm = 123;
    data.longitude =  10.5482161*100000000;
    data.latitude = 52.31402124*100000000;
    data.velocity[0] = 0.0;
    data.velocity[1] = 0.0;
    data.velocity[2] = 0.0;
    data.yaw_degree = 10.3;
    data.pitch_degree = 4;
    data.roll_degree = 0.42;
    
    // Schreiben des structs in die zuvor geoeffnete Message-Queue
    mq_send(queue, (void *)&data, sizeof(data), 0);
    
    // Aufruf der App, welche nun, wenn moeglich die Daten sendet.
    int ret;
    ret = senser_main(UAVCAN_SEND, NULL);
    if (ret < 0 ) {
      printf("Daten konnten nicht gesendet werden");
    }
}
```

### Stoppen

Zum stoppen der App wird der App mitgeteilt, dass sie beendet werden soll. Weiterhin sollte die Message Queue geschlossen werden.

```C++
void stopApp() {
    // Aufruf der App, welche nun, wenn moeglich die Daten sendet.
    int ret;
    ret = senser_main(UAVCAN_STOP, NULL);
    if (ret < 0 ) {
      printf("App konnte nicht gestoppt werden");
    }
    // Schließen der Message Queue
    mq_close(queue);
}
```
 


## UAVCAN Daten

Zur Übertragung der Sensordaten werden folgende UAVCAN-Nachrichten verwendet:

* MagneticFieldStrength
* Fix
* Auxiliary
* StaticPressure
* StaticTemperature

Weiterhin benötigt das Protokoll die Nachrichten:

* NodeStatus
* Allocation

Die NodeStatus Nachricht beinhaltet Informationen zum Status des UAVCAN-Nodes. Die Allocation-Nachricht ist nötig, um zu Beginn der Übertragung eine noch nicht vorhandene Id zu erhalten.

Die Definition der einzelnen Nachrichten nach dem UAVCAN-Protokoll ist wie folgt:

### ```MagneticFieldStrength```

Full name: uavcan.equipment.ahrs.MagneticFieldStrength

Default data type ID: 1001
```Python
#
# Magnetic field readings, in Gauss, in body frame.
# SI units are avoided because of float16 range limitations.
# This message is deprecated. Use the newer 1002.MagneticFieldStrength2.uavcan message.
#

float16[3] magnetic_field_ga
float16[<=9] magnetic_field_covariance
```

### ```Fix```

Full name: uavcan.equipment.gnss.Fix

Default data type ID: 1060
```Python
#
# GNSS navigation solution with uncertainty.
# This message is deprecated. Use the newer 1063.Fix2.uavcan message.
#

uavcan.Timestamp timestamp         # Global network-synchronized time, if available, otherwise zero

#
# Time solution.
# Time standard (GPS, UTC, TAI, etc) is defined in the field below.
#
uavcan.Timestamp gnss_timestamp

#
# Time standard used in the GNSS timestamp field.
#
uint3 GNSS_TIME_STANDARD_NONE = 0  # Time is unknown
uint3 GNSS_TIME_STANDARD_TAI  = 1
uint3 GNSS_TIME_STANDARD_UTC  = 2
uint3 GNSS_TIME_STANDARD_GPS  = 3
uint3 gnss_time_standard

void5   # Reserved space

#
# If known, the number of leap seconds allows to perform conversions between some time standards.
#
uint8 NUM_LEAP_SECONDS_UNKNOWN = 0
uint8 num_leap_seconds

#
# Position and velocity solution
#
int37 longitude_deg_1e8            # Longitude degrees multiplied by 1e8 (approx. 1 mm per LSB)
int37 latitude_deg_1e8             # Latitude degrees multiplied by 1e8 (approx. 1 mm per LSB on equator)
int27 height_ellipsoid_mm          # Height above ellipsoid in millimeters
int27 height_msl_mm                # Height above mean sea level in millimeters

float16[3] ned_velocity            # NED frame (north-east-down) in meters per second

#
# Fix status
#
uint6 sats_used

uint2 STATUS_NO_FIX    = 0
uint2 STATUS_TIME_ONLY = 1
uint2 STATUS_2D_FIX    = 2
uint2 STATUS_3D_FIX    = 3
uint2 status

#
# Precision
#
float16 pdop

void4
float16[<=9] position_covariance   # m^2
float16[<=9] velocity_covariance   # (m/s)^2
```
### ```Auxiliary```

Full name: uavcan.equipment.gnss.Auxiliary

Default data type ID: 1061
```Python
#
# GNSS low priority auxiliary info.
# Unknown DOP parameters should be set to NAN.
#

float16 gdop
float16 pdop
float16 hdop
float16 vdop
float16 tdop
float16 ndop
float16 edop

uint7 sats_visible                    # All visible sats of all available GNSS (e.g. GPS, GLONASS, etc)
uint6 sats_used                       # All used sats of all available GNSS
```

### ```StaticPressure```

Full name: uavcan.equipment.air_data.StaticPressure

Default data type ID: 1028
```
#
# Static pressure.
#

float32 static_pressure                 # Pascal
float16 static_pressure_variance        # Pascal^2
```
### ```StaticTemperature```

Full name: uavcan.equipment.air_data.StaticTemperature

Default data type ID: 1029
```
#
# Static temperature.
#

float16 static_temperature              # Kelvin
float16 static_temperature_variance     # Kelvin^2
```

### ```NodeStatus```

Full name: uavcan.protocol.NodeStatus

Default data type ID: 341

```
#
# Abstract node status information.
#
# Any UAVCAN node is required to publish this message periodically.
#

#
# Publication period may vary within these limits.
# It is NOT recommended to change it at run time.
#
uint16 MAX_BROADCASTING_PERIOD_MS = 1000
uint16 MIN_BROADCASTING_PERIOD_MS = 2

#
# If a node fails to publish this message in this amount of time, it should be considered offline.
#
uint16 OFFLINE_TIMEOUT_MS = 3000

#
# Uptime counter should never overflow.
# Other nodes may detect that a remote node has restarted when this value goes backwards.
#
uint32 uptime_sec

#
# Abstract node health.
#
uint2 HEALTH_OK         = 0     # The node is functioning properly.
uint2 HEALTH_WARNING    = 1     # A critical parameter went out of range or the node encountered a minor failure.
uint2 HEALTH_ERROR      = 2     # The node encountered a major failure.
uint2 HEALTH_CRITICAL   = 3     # The node suffered a fatal malfunction.
uint2 health

#
# Current mode.
#
# Mode OFFLINE can be actually reported by the node to explicitly inform other network
# participants that the sending node is about to shutdown. In this case other nodes will not
# have to wait OFFLINE_TIMEOUT_MS before they detect that the node is no longer available.
#
# Reserved values can be used in future revisions of the specification.
#
uint3 MODE_OPERATIONAL      = 0         # Node is performing its main functions.
uint3 MODE_INITIALIZATION   = 1         # Node is initializing; this mode is entered immediately after startup.
uint3 MODE_MAINTENANCE      = 2         # Node is under maintenance.
uint3 MODE_SOFTWARE_UPDATE  = 3         # Node is in the process of updating its software.
uint3 MODE_OFFLINE          = 7         # Node is no longer available.
uint3 mode

#
# Not used currently, keep zero when publishing, ignore when receiving.
#
uint3 sub_mode

#
# Optional, vendor-specific node status code, e.g. a fault code or a status bitmask.
#
uint16 vendor_specific_status_code
```

### ```Allocation```

Full name: uavcan.protocol.dynamic_node_id.Allocation

Default data type ID: 1
```
#
# This message is used for dynamic Node ID allocation.
#
# When a node needs to request a node ID dynamically, it will transmit an anonymous message transfer of this type.
# In order to reduce probability of CAN ID collisions when multiple nodes are publishing this request, the CAN ID
# field of anonymous message transfer includes a Discriminator, which is a special field that has to be filled with
# random data by the transmitting node. Since Discriminator collisions are likely to happen (probability approx.
# 0.006%), nodes that are requesting dynamic allocations need to be able to handle them correctly. Hence, a collision
# resolution protocol is defined (alike CSMA/CD). The collision resolution protocol is based on two randomized
# transmission intervals:
#
# - Request period - Trequest.
# - Followup delay - Tfollowup.
#
# Recommended randomization ranges for these intervals are documented in the costants of this message type (see below).
# Random intervals must be chosen anew per transmission, whereas the Discriminator value is allowed to stay constant
# per node.
#
# In the below description the following terms are used:
# - Allocator - the node that serves allocation requests.
# - Allocatee - the node that requests an allocation from the Allocator.
#
# The response timeout is not explicitly defined for this protocol, as the Allocatee will request the allocation
# Trequest units of time later again, unless the allocation has been granted. Despite this, the implementation can
# consider the value of FOLLOWUP_TIMEOUT_MS as an allocation timeout, if necessary.
#
# On the allocatee's side the protocol is defined through the following set of rules:
#
# Rule A. On initialization:
# 1. The allocatee subscribes to this message.
# 2. The allocatee starts the Request Timer with a random interval of Trequest.
#
# Rule B. On expiration of Request Timer:
# 1. Request Timer restarts with a random interval of Trequest.
# 2. The allocatee broadcasts a first-stage Allocation request message, where the fields are assigned following values:
#    node_id                 - preferred node ID, or zero if the allocatee doesn't have any preference
#    first_part_of_unique_id - true
#    unique_id               - first MAX_LENGTH_OF_UNIQUE_ID_IN_REQUEST bytes of unique ID
#
# Rule C. On any Allocation message, even if other rules also match:
# 1. Request Timer restarts with a random interval of Trequest.
#
# Rule D. On an Allocation message WHERE (source node ID is non-anonymous) AND (allocatee's unique ID starts with the
# bytes available in the field unique_id) AND (unique_id is less than 16 bytes long):
# 1. The allocatee waits for Tfollowup units of time, while listening for other Allocation messages. If an Allocation
#    message is received during this time, the execution of this rule will be terminated. Also see rule C.
# 2. The allocatee broadcasts a second-stage Allocation request message, where the fields are assigned following values:
#    node_id                 - same value as in the first-stage
#    first_part_of_unique_id - false
#    unique_id               - at most MAX_LENGTH_OF_UNIQUE_ID_IN_REQUEST bytes of local unique ID with an offset
#                              equal to number of bytes in the received unique ID
#
# Rule E. On an Allocation message WHERE (source node ID is non-anonymous) AND (unique_id fully matches allocatee's
# unique ID) AND (node_id in the received message is not zero):
# 1. Request Timer stops.
# 2. The allocatee initializes its node_id with the received value.
# 3. The allocatee terminates subscription to Allocation messages.
# 4. Exit.
#

#
# Recommended randomization range for request period.
#
# These definitions have an advisory status; it is OK to pick higher values for both bounds, as it won't affect
# protocol compatibility. In fact, it is advised to pick higher values if the target application is not concerned
# about the time it will spend on completing the dynamic node ID allocation procedure, as it will reduce
# interference with other nodes, possibly of higher importance.
#
# The lower bound shall not be lower than FOLLOWUP_TIMEOUT_MS, otherwise the request may conflict with a followup.
#
uint16 MAX_REQUEST_PERIOD_MS = 1000     # It is OK to exceed this value
uint16 MIN_REQUEST_PERIOD_MS = 600      # It is OK to exceed this value

#
# Recommended randomization range for followup delay.
# The upper bound shall not exceed FOLLOWUP_TIMEOUT_MS, because the allocator will reset the state on its end.
#
uint16 MAX_FOLLOWUP_DELAY_MS = 400
uint16 MIN_FOLLOWUP_DELAY_MS = 0        # Defined only for regularity; will always be zero.

#
# Allocator will reset its state if there was no follow-up request in this amount of time.
#
uint16 FOLLOWUP_TIMEOUT_MS = 500

#
# Any request message can accommodate no more than this number of bytes of unique ID.
# This limitation is needed to ensure that all request transfers are single-frame.
# This limitation does not apply to CAN FD transport.
#
uint8 MAX_LENGTH_OF_UNIQUE_ID_IN_REQUEST = 6

#
# When requesting an allocation, set the field 'node_id' to this value if there's no preference.
#
uint7 ANY_NODE_ID = 0

#
# If transfer is anonymous, this is the preferred ID.
# If transfer is non-anonymous, this is allocated ID.
#
# If the allocatee does not have any preference, this value must be set to zero. In this case, the allocator
# must choose the highest unused node ID value for this allocation (except 126 and 127, that are reserved for
# network maintenance tools). E.g., if the allocation table is empty and the node has requested an allocation
# without any preference, the allocator will grant the node ID 125.
#
# If the preferred node ID is not zero, the allocator will traverse the allocation table starting from the
# prefferred node ID upward, untill a free node ID is found. If a free node ID could not be found, the
# allocator will restart the search from the preferred node ID downward, until a free node ID is found.
#
# In pseudocode:
#   int findFreeNodeID(const int preferred)
#   {
#       // Search up
#       int candidate = (preferred > 0) ? preferred : 125;
#       while (candidate <= 125)
#       {
#           if (!isOccupied(candidate))
#               return candidate;
#           candidate++;
#       }
#       // Search down
#       candidate = (preferred > 0) ? preferred : 125;
#       while (candidate > 0)
#       {
#           if (!isOccupied(candidate))
#               return candidate;
#           candidate--;
#       }
#       // Not found
#       return -1;
#   }
#
uint7 node_id

#
# If transfer is anonymous, this field indicates first-stage request.
# If transfer is non-anonymous, this field should be assigned zero and ignored.
#
bool first_part_of_unique_id

#
# If transfer is anonymous, this array must not contain more than MAX_LENGTH_OF_UNIQUE_ID_IN_REQUEST items.
# Note that array is tail-optimized, i.e. it will not be prepended with length field.
#
uint8[<=16] unique_id
```
