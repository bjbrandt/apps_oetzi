/****************************************************************************
 * examples/cosmic_scheduler/cosmic_scheduler.c
 *
 *   Copyright (C) 2008, 2011-2012 Gregory Nutt. All rights reserved.
 *   Author: Gregory Nutt <gnutt@nuttx.org>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>

#include <sys/ioctl.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <signal.h>
#include <errno.h>
#include <mqueue.h>
#include <nuttx/timers/timer.h>

#include <semaphore.h>
#include <mqueue.h>

#include <nuttx/timers/hptc.h>
/*IMU*/
#include <nuttx/sensors/adis16xxx.h>
/*led*/
#include <nuttx/leds/userled.h>


/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

//#define CONFIG_COSMIC_SCHEDULER_DEVNAME "/dev/timer12"
//#define CONFIG_COSMIC_SCHEDULER_INTERVAL 10000
//#define CONFIG_COSMIC_SCHEDULER_SIGNO 17
//#define CONFIG_COSMIC_SCHEDULER_DELAY 10000

//#define CHILD_ARG ((void*)0x12345678)

//int child_apid;


//dynamixel
#ifdef CONFIG_EXAMPLES_DYNAMIXEL
#define DYNAMIXEL_PERIODIC_READ_ADR 0x1E
#define DYNAMIXEL_PERIODIC_READ_LEN 17
#endif



#ifdef CONFIG_EXAMPLES_SENSOR
#define UAVCAN_START 		0x00
#define UAVCAN_SEND 		0x01
#define UAVCAN_STOP 		0x02
#endif

#ifdef CONFIG_EXAMPLES_SENSOR
struct uavcanData_s{
	uint64_t timestamp;
	int32_t height_mm;
	int64_t longitude_1e8;
	int64_t latitude_1e8;
	float velocity[3];
	float yaw_degree;
	float pitch_degree;
	float roll_degree;
};

static mqd_t mqd_uavcan;

#endif


#undef PROFILING

#ifdef PROFILING
struct timespec time_b[100];
struct timespec time_a[100];
struct timespec time_c[100];
struct timespec time_d[100];

int i_time=0;
#endif

/****************************************************************************
 * Public Functions
 ****************************************************************************/
//static volatile unsigned long g_nsignals;

#ifdef CONFIG_EXAMPLES_DYNAMIXEL
int dynamixel_rx(int fd, uint8_t *s, uint8_t len, uint8_t *rx_id, uint8_t *err)
{
	  uint8_t val;
	  uint8_t chk=0;
	  int ret;
	  int cnt=0;
//static int valid=0;
//static int too_short=0;
//static int prescaler=0;

	  ret = ioctl(fd, FIONREAD,(unsigned long)&cnt); //trigger buffer synchronisation.
	  //printf("ioctl: (cnt=len+6) cnt=%d, len=%hhu\n",cnt,len);
#if 0
if(prescaler==100){
      printf("cnt=%d, len=%d, valid=%d, short:%d\n",cnt, len, valid,too_short);
      valid=0;
      too_short=0;
      int i=0;
      for(i=0;i<cnt;i++){
    	  ret=read(fd,&val,1);
    	  if(ret==1)printf("%01d:%02x\n",i,val);
      }
prescaler=0;
return -1;

}
#endif
//if(cnt<(len+6))too_short++;
//prescaler++;
	  while(cnt>=(len+6)){

		/*first magic byte*/
		ret=read(fd,&val,1);

		if (ret !=1){
			//printf("read error\n");
			return -1;
		}
		//printf("first magic: 0x%hhx\n",val);
		cnt--;
		if (val != 0xFF) continue;

		/*second magic byte*/
		ret=read(fd,&val,1);
		if (ret !=1){
			//printf("read error\n");
			return -1;
		}
		//printf("second magic: 0x%hhx\n",val);
		cnt--;
		if (val != 0xFF) continue;

		/* msg_id */
		ret=read(fd,&val,1);
		if (ret !=1){
			//printf("read error\n");
			return -1;
		}
		//printf("msg_id: 0x%hhx\n",val);
		cnt--;
	    *rx_id=val;
        chk=val;

        /*msg_length*/
		ret=read(fd,&val,1);
		if (ret !=1){
			//printf("read error\n");
			return -1;
		}
		//printf("msg_len: %hhd\n",val);
		cnt--;
		if ((len+2) != val) continue;
		chk+=val;

	    //msg_err byte
	    ret=read(fd,&val,1);
	    if (ret !=1) {
	    	//printf("read error\n");
			return -1;
		}
		//printf("err_byte: 0x%hhX\n",val);
	    cnt--;
	    *err=val;
	    chk+=val;


		  int i;
		  for(i=0; i<len; i++){
			  ret=read(fd,&val,1);
			  if (ret !=1) {
				  //printf("read error\n");
					return -1;
				}
		//printf("data [%02d]: 0x%hhX\n",i,val);
			  cnt--;
			  s[i]=val;
			  chk+=val;
		  }

	  //msg_crc byte
	  ret=read(fd,&val,1);
	  if (ret !=1){
		  //printf("read error\n");
			return -1;
		}
	  cnt--;
	  chk+=val;
	  if(( chk&0xff) != 0xff) {
		  //printf( "sum: 0x%02X\r\n",chk);
		  continue;
	  } else {
//valid++;
		  //printf("valid data\n");
		  return 0;
	  }
    }
	while(cnt){ //flush buffer
		ret=read(fd,&val,1);
			  if (ret !=1){
					//printf("read error\n");
					return -1;
				}
			  cnt--;
	}
	return -1;

}




int dynamixel_read_request(int fd, uint8_t id, uint8_t adr, uint8_t len){
	  uint8_t tx_buf[8];
	  uint8_t chk=0;
	  int ret;
	  //uint8_t err;
	  uint8_t rx_id;
	  tx_buf[0]=0xFF;
	  tx_buf[1]=0xFF;
	  tx_buf[2]=id; //id: broadcast = 0xFE
	  tx_buf[3]=0x04; //length
	  tx_buf[4]=0x02; //instruction:read
	  tx_buf[5]=adr; //ADR
	  tx_buf[6]=len; //LEN
	  for(int i=2;i<7;i++){
		  chk+=tx_buf[i];
	  }
	  tx_buf[7]=~chk;
	  ret=write (fd, tx_buf, 8);
	  if (ret!=8) return -1;
	  return 0;
}


int dynamixel_read(int fd, uint8_t id, uint8_t adr, uint8_t len, uint8_t* buf, uint8_t *err){

	  int ret;
	  uint8_t rx_id;
	  ret=dynamixel_read_request(fd,id,adr,len);
	  if(ret != 0) return ret;

	  //sleep(1); //FIXME
	  usleep(500);

	  ret= dynamixel_rx(fd, buf, len, &rx_id, err);

	  //printf("ret: %d, id: 0x%02X, err: 0x%02X\r\n",ret, (unsigned int)rx_id, (unsigned int)err);
      return ret;

}





int dynamixel_read8(int fd, uint8_t id, uint8_t adr, uint8_t* data, uint8_t *err){
	  return dynamixel_read(fd, id, adr, 1, data, err);
}

int dynamixel_read16(int fd, uint8_t id, uint8_t adr, uint16_t* data, uint8_t *err){
	  return dynamixel_read(fd, id, adr, 2, (uint8_t*)data, err);
}






static void dynamixel_write8(int fd, uint8_t id, uint8_t addr, uint8_t data){
	  uint8_t tx_buf[8];
	  uint8_t chk=0;
	  int i=0;

	  tx_buf[0]=0xFF;
	  tx_buf[1]=0xFF;
	  tx_buf[2]=id;
	  tx_buf[3]=4; //length
	  tx_buf[4]=0x03; //instruction:write
	  tx_buf[5]=addr; //ADR
	  tx_buf[6]=data;

	  for(i=2;i<7;i++){
		  chk+=tx_buf[i];
	  }
	  tx_buf[7]=~chk;
	  write(fd, tx_buf, 8);
//	  dynamixel_tx (tx_buf, 8);
}






static void dynamixel_write16(int fd, uint8_t id, uint8_t addr, uint16_t data){
	  uint8_t tx_buf[9];
	  uint8_t chk=0;
	  int i=0;

	  tx_buf[0]=0xFF;
	  tx_buf[1]=0xFF;
	  tx_buf[2]=id;
	  tx_buf[3]=5; //length
	  tx_buf[4]=0x03; //instruction:write
	  tx_buf[5]=addr; //ADR
	  *(uint16_t *)(&tx_buf[6])=data;

	  for(i=2;i<8;i++){
		  chk+=tx_buf[i];
	  }
	  tx_buf[8]=~chk;

	  write(fd, tx_buf, 9);
}
#endif


/****************************************************************************
 * cosmic_scheduler_main
 ****************************************************************************/



#ifdef CONFIG_BUILD_KERNEL
int main(int argc, FAR char *argv[])
#else
int cosmic_scheduler_main(int argc, char *argv[])
#endif
{
#if 0
  struct timer_notify_s notify;
  struct sigaction act;
  struct timer_status_s status;
#endif
  int ret;
  int fd_hptc;
  struct hptc_ic_msg_s ic_data;
  sem_t* sem_hptc;
  sem_t* sem_nutros_tx;
  sem_t* sem_uavcan;

  mqd_t mqd_imu;
  struct mq_attr mq_imu_attr;

#ifdef CONFIG_EXAMPLES_DYNAMIXEL
  uint8_t dynamixel_data[8+DYNAMIXEL_PERIODIC_READ_LEN];
  uint8_t dynamixel_cmd_data[4];
  mqd_t mqd_dynamixel;
  mqd_t mqd_dynamixel_cmd;
  struct mq_attr mq_dynamixel_attr;
  struct mq_attr mq_dynamixel_cmd_attr;

  uint8_t pulse_ts_data[10];
  mqd_t   mqd_pulse_ts;
  struct  mq_attr mq_pulse_ts_attr;

#endif

  int fd_imu;
  struct adis16488_msg_s imu_data;
  uint16_t prod_id;

  FAR struct timespec timestamp;
//  FAR struct timespec semtimestamp;
//  pthread_attr_t attr;
//  pthread_t thread;


//  volatile uint32_t* gpiob_bssr=(void*)0x40020418;



#if 0
  int fd_led;
  userled_set_t supported;
  fd_led = open("/dev/userleds", O_WRONLY);
    if (fd_led < 0)
      {
        int errcode = errno;
        printf("led: ERROR: Failed to open %s: %d\n",
        		"/dev/userleds", errcode);

       sleep (2);
       return -1;
      }

    /* Get the set of LEDs supported */

    ret = ioctl(fd_led, ULEDIOC_SUPPORTED,
                (unsigned long)((uintptr_t)&supported));
    if (ret < 0)
      {
        int errcode = errno;
        printf("led_daemon: ERROR: ioctl(ULEDIOC_SUPPORTED) failed: %d\n",
               errcode);
        sleep(2);
        return -1;
      }

    /* Excluded any LEDs that not supported AND not in the set of LEDs the
     * user asked us to use.
     */

    printf("led: Supported LEDs 0x%02x\n", (unsigned int)supported);

    ret = ioctl(fd_led, ULEDIOC_SETALL, 0x01);

    sleep(2);

    ret = ioctl(fd_led, ULEDIOC_SETALL, 0x02);


    sleep(2);

    ret = ioctl(fd_led, ULEDIOC_SETALL, 0x00);

    sleep(2);
#endif

#if 0
    *gpiob_bssr=0x00000002;

    sleep(3);

    *gpiob_bssr=0x00000004;

    sleep(2);

    *gpiob_bssr=0x00060000;

    sleep(1);
    *gpiob_bssr=0x00000006;

    sleep(2);
#endif

#ifdef CONFIG_EXAMPLES_DYNAMIXEL
  /*dynamixel*/
  int fd_dyn;
  uint8_t err;
  uint16_t data16;
  uint8_t data8;


  printf("Open /dev/ttyS0\n");
  sleep(2);
  fd_dyn = open("/dev/ttyS0", O_RDWR | O_NONBLOCK);
  //fd_dyn = open("/dev/ttyS0", O_RDWR);

  if (fd_dyn < 0){
	  fprintf(stderr, "ERROR: Failed to open /dev/ttyS0: %d\n",
			  errno);
      return EXIT_FAILURE;
  }


#if 1

  ret=dynamixel_read16(fd_dyn, 1, 0x00, &data16, &err);
  if(ret){
	  printf ("Model: error reading dynamixel\n");
  } else {
   printf("Model:   0x%04hX, err:%hhu\n",data16, err);
  }


  ret=dynamixel_read8(fd_dyn, 1, 0x02, &data8, &err);
  if(ret){
	  printf ("FW: error reading dynamixel\n");
  } else {
   printf("FW:      0x%02hhX, err:%hhu\n",data8, err);
  }

   ret=dynamixel_read8(fd_dyn, 1, 0x03, &data8, &err);
   if(ret){
 	  printf ("ID: error reading dynamixel\n");
   } else {
   printf("ID:      0x%02hhX, err:%hhu\n",data8, err);
   }

   ret=dynamixel_read8(fd_dyn, 1,0x04, &data8,&err);
   if(ret){
 	  printf ("Baud: error reading dynamixel\n");
   } else {
   printf("Baud:    0x%02hhX, err:%hhu\n",data8, err);
   }


   //sleep (1);
   //dynamixel_write8(fd_dyn,1,0x04,252); //set baudrate to 3MBit/s (value 252)
   //sleep(1);




   ret=dynamixel_read8(fd_dyn, 1,0x05, &data8,&err);
   if(ret){
 	  printf ("Return delay error reading dynamixel\n");
   } else {
    printf("Return Delay:    %hhu, err:%hhu\n",data8, err);
    if (data8!=20){
  	  dynamixel_write8(fd_dyn, 1,0x05,20);
  	  sleep(1);
  	  dynamixel_read8(fd_dyn, 1,0x05, &data8,&err);
  	  printf("Return Delay now:    %hhu, err:%hhu\n",data8, err);
    }
   }

    ret=dynamixel_read16(fd_dyn, 1, 0x06, &data16, &err);
    if(ret){
  	  printf ("CW limit: error reading dynamixel\n");
    } else {
    printf("CW limit:   0x%04hX, err:%hhu\n",data16, err);
    if (data16!=0xFFF){
  	  dynamixel_write16(fd_dyn, 1,0x06,0xfff);
  	  sleep(1);
  	  dynamixel_read16(fd_dyn, 1, 0x06, &data16, &err);
  	  printf("CW limit now:   0x%04hX, err:%hhu\n",data16, err);
    }
    }
    ret=dynamixel_read16(fd_dyn, 1, 0x08, &data16, &err);
    if(ret){
  	  printf ("CCW limit: error reading dynamixel\n");
    } else {
    printf("CCW limit:   0x%04hX, err:%hhu\n",data16, err);
    if (data16!=0xFFF){
  	  dynamixel_write16(fd_dyn, 1,0x08,0xfff);
  	  sleep(1);
  	  dynamixel_read16(fd_dyn, 1, 0x08, &data16, &err);
  	  printf("CCW limit now:   0x%04hX, err:%hhu\n",data16, err);
    }
    }

    ret=dynamixel_read8(fd_dyn, 1, 0x0B, &data8, &err);
    if(ret){
  	  printf ("T limit: error reading dynamixel\n");
    } else {
    printf("T limit:      %hhu, err:%hhu\n",data8, err);
    }
    ret=dynamixel_read8(fd_dyn, 1,0x0C, &data8,&err);
    if(ret){
  	  printf ("U low: error reading dynamixel\n");
    } else {
    printf("U low:    0x%02X, err:%hhu\n",(int)data8, (unsigned int)err);
    }
    ret=dynamixel_read8(fd_dyn, 1,0x0D, &data8,&err);
    if(ret){
  	  printf ("U high: error reading dynamixel\n");
    } else {
    printf("U high:    0x%02X, err:%hhu\n",(int)data8, (int)err);
    }
    ret=dynamixel_read16(fd_dyn, 1, 0x0E, &data16, &err);
    if(ret){
  	  printf ("torque limit: error reading dynamixel\n");
    } else {
    printf("torque limit:   0x%04X, err:%hhu\n",(int)data16, (int)err);
    }
    ret=dynamixel_read8(fd_dyn, 1,0x10, &data8,&err);
    if(ret){
  	  printf ("Status Return Level: error reading dynamixel\n");
    } else {
    printf("Status Return Level:    0x%02hhX, err:%hhu\n",(int)data8, (int)err);
    if (data8!=1){
  	  dynamixel_write8(fd_dyn, 1,0x10,1);
  	  sleep(1);
  	  dynamixel_read8(fd_dyn, 1,0x10, &data8,&err);
  	  printf("Status Return Level now:    0x%02hhX, err:%hhu\n",(int)data8, (int)err);
    }
    }

    ret=dynamixel_read8(fd_dyn, 1,0x11, &data8,&err);
    if(ret){
  	  printf ("Alarm LED: error reading dynamixel\n");
    } else {
    printf("Alarm LED:    0x%02hhX, err:%hhu\n",(int)data8, (int)err);
    }
    ret=dynamixel_read8(fd_dyn, 1,0x12, &data8,&err);
    if(ret){
  	  printf ("Alarm shutdown: error reading dynamixel\n");
    } else {
    printf("Alarm shutdown:    0x%02hhX, err:%hhu\n",(int)data8, (int)err);
    }

    ret=dynamixel_read16(fd_dyn, 1, 0x14, &data16, &err);
    if(ret){
  	  printf ("Multiturn offset: error reading dynamixel\n");
    } else {
      printf("multiturn offset:   0x%04hX, err:%hhu\n",(int)data16, (int)err);
      if (data16!=0){
  	    dynamixel_write16(fd_dyn, 1, 0x14, 0);
	    sleep(1);
	    ret=dynamixel_read16(fd_dyn, 1, 0x14, &data16, &err);
	    printf("multiturn offset now:   0x%04hX, err:%hhu\n",(int)data16, (int)err);
      }
    }
    ret=dynamixel_read8(fd_dyn, 1,0x16, &data8,&err);
    if(ret){
  	  printf ("resolution divider: error reading dynamixel\n");
    } else {
    printf("resolution divider:    0x%02hhX, err:%hhu\n",(int)data8, (int)err);
    }



#if 0
   printf("done\n");
for(;;){

	sleep(1);

}
#endif


#endif

#endif




  /* Open the timer device */

  printf("Open %s\n", CONFIG_HPTC_DEV_PATH);
sleep(1);

  fd_hptc = open(CONFIG_HPTC_DEV_PATH, O_RDONLY | O_NONBLOCK);
  if (fd_hptc < 0)
    {
	  fprintf(stderr, "ERROR: Failed to open %s: %d\n",
			  CONFIG_HPTC_DEV_PATH, errno);
      return EXIT_FAILURE;
    }



    printf("get hptc semaphore\n");
sleep(1);
    ret = ioctl(fd_hptc, HPTCIOC_GETSEM, (long int)&sem_hptc);
    if (ret < 0)
      {
        fprintf(stderr, "ERROR: Failed to get hptc semaphore: %d\n", errno);
        close(fd_hptc);
        return EXIT_FAILURE;
      }

//    int sem_val=0;
//    sem_getvalue(sem_hptc,&sem_val);
//    printf("sem: val=%d\n",sem_val);

    //------------------------------------------------------------------------------------------


     /* Start the timer */

     printf("Start the timer\n");
sleep(1);
     ret = ioctl(fd_hptc, HPTCIOC_START, 0);
     if (ret < 0)
       {
         fprintf(stderr, "ERROR: Failed to start the hptc: %d\n", errno);
         close(fd_hptc);
         return EXIT_FAILURE;
       }

printf("timer started.\n");
sleep(1);

#if 1
struct timespec ts1,ts2;
clock_gettime(CLOCK_REALTIME, &ts1);
clock_gettime(CLOCK_REALTIME, &ts2);

printf("ts1: %09d,%09d\n",ts1.tv_sec, ts1.tv_nsec);
printf("ts2: %09d,%09d\n",ts2.tv_sec, ts2.tv_nsec);
sleep(1);
#endif

     mq_imu_attr.mq_maxmsg  = 2;
     mq_imu_attr.mq_msgsize = sizeof(struct adis16488_msg_s);
     mq_imu_attr.mq_flags   = 0;

     printf("create mq_imu, msg_size: %d\n",sizeof(struct adis16488_msg_s) );

     mqd_imu = mq_open("mq_imu",O_CREAT | O_WRONLY | O_NONBLOCK, 0666, &mq_imu_attr);

	 if(mqd_imu < 0){
		 printf("creating mqd_imu failed \n");
		 return EXIT_FAILURE;
	 }else{
		 printf("mqd_imu created: %i \n",mqd_imu);
	 }
//     sleep(1);



#ifdef CONFIG_EXAMPLES_DYNAMIXEL
     mq_dynamixel_attr.mq_maxmsg  = 2;
     mq_dynamixel_attr.mq_msgsize = sizeof(dynamixel_data);
     mq_dynamixel_attr.mq_flags   = 0;

     printf("create mq_dynamixel, msg_size: %d\n",sizeof(dynamixel_data) );

     mqd_dynamixel = mq_open("mq_dynamixel",O_CREAT | O_WRONLY | O_NONBLOCK, 0666, &mq_dynamixel_attr);

	 if(mqd_dynamixel < 0){
		 printf("creating mqd_dynamixel failed \n");
		 return EXIT_FAILURE;
	 }else{
		 printf("mqd_dynamixel created: %i \n",mqd_dynamixel);
	 }



     mq_dynamixel_cmd_attr.mq_maxmsg  = 2;
     mq_dynamixel_cmd_attr.mq_msgsize = sizeof(dynamixel_cmd_data);
     mq_dynamixel_cmd_attr.mq_flags   = 0;

     printf("create mq_dynamixel_cmd, msg_size: %d\n",sizeof(dynamixel_cmd_data) );

     mqd_dynamixel_cmd = mq_open("mq_dynamixel_cmd",O_CREAT | O_RDONLY | O_NONBLOCK, 0666, &mq_dynamixel_cmd_attr);

	 if(mqd_dynamixel_cmd < 0){
		 printf("creating mqd_dynamixel_cmd failed \n");
		 return EXIT_FAILURE;
	 }else{
		 printf("mqd_dynamixel_cmd created: %i \n",mqd_dynamixel_cmd);
	 }


	 mq_pulse_ts_attr.mq_maxmsg = 4;
	 mq_pulse_ts_attr.mq_msgsize = sizeof(pulse_ts_data);
	 mq_pulse_ts_attr.mq_flags   = 0;

     printf("create mq_pulse_ts msg_size: %d\n",sizeof(pulse_ts_data) );

     mqd_pulse_ts = mq_open("mq_pulse_ts",O_CREAT | O_WRONLY | O_NONBLOCK, 0666, &mq_pulse_ts_attr);

  	 if(mqd_pulse_ts < 0){
  		 printf("creating mqd_pulse_ts failed \n");
  		 return EXIT_FAILURE;
  	 }else{
  		 printf("mqd_pulse_ts created: %i \n",mqd_pulse_ts);
  	 }


#endif




#ifdef CONFIG_EXAMPLES_SENSOR


	    printf("open sem_uavcan semaphore...\n");

	    sem_uavcan = sem_open("uavcan",O_CREAT,0,0);
	     printf("ret sem_uavcan: %i \n", sem_uavcan);
	     /*warten*/
	//     sleep(1);

	     ret = sem_setprotocol(sem_uavcan, SEM_PRIO_NONE);
	      if (ret < 0)
	        {
	           printf("ERROR: sem_setprotocol failed: %d\n", ret);
	           return EXIT_FAILURE;
	        }



	 printf("run uav-can sensor_main\n");
	 sleep(1);
     sensor_main();
#if 0
	 mqd_uavcan = mq_open("mq_uavcan",O_NONBLOCK | O_WRONLY);
	 if((int)mqd_uavcan < 0){
		 printf("mqd_uavcan failed \n");
	 }else{
		 printf("mqd_uavcan success: %i \n",mqd_uavcan);
	 }
#endif


#endif






    printf("open nutros_tx semaphore...\n");

     sem_nutros_tx = sem_open("nutros_tx",O_CREAT,0,0);
     printf("ret sem_nutros_tx: %i \n", sem_nutros_tx);
     /*warten*/
//     sleep(1);

     ret = sem_setprotocol(sem_nutros_tx, SEM_PRIO_NONE);
      if (ret < 0)
        {
           printf("ERROR: sem_setprotocol failed: %d\n", ret);
           return EXIT_FAILURE;
        }

#if 1
//     printf("run nutros_a_main(), waiting for USB connection...\n");
//     sleep(1);
     nutros_a_main();
     printf("Nutros running.\n");
//     sleep(1);
#endif


#if 1
     /*open imu*/
     printf("Open imu\n");
     fd_imu = open("/dev/imu0",O_RDWR);
     if(fd_imu<0){
       printf("error opening /dev/imu0\n");
     } else {
       ret=ioctl(fd_imu,SNIOC_WHO_AM_I,&prod_id);
       printf("imu product id: %d\n",prod_id);

     }
#endif









 printf("run...\n");
 //sleep(2);

#if 0
     for(int i=0; i<10;i++){
    	 struct hptc_status_s hptc_status;
    	 ret = ioctl(fd_hptc, HPTCIOC_GETSTATUS, (long int) &hptc_status);
    	 printf("%2d:cnt:%10u; cnt1:%10u; cnt2:%10u\n", i,hptc_status.flags,hptc_status.cnt1,hptc_status.cnt2);
    	 sleep(1);
     }
#endif


     bool slice=true;
     int i=0;
//     int j=0;
     int iax=0;
     int iay=0;
     int iaz=0;

     for(;;){
    	 sem_wait(sem_hptc); //2khz hardcoded in hptc at time of writing this file

#ifdef PROFILING
clock_gettime(CLOCK_REALTIME, &time_b[i_time]);
ret = ioctl(fd_hptc, HPTCIOC_GETSEMTIME, (long int)&time_a[i_time]);
#endif


    	 if(slice){

    		 //clock_gettime(CLOCK_REALTIME, &timestamp);
    		 ret = ioctl(fd_hptc, HPTCIOC_GETSEMTIME, (long int)&timestamp);

             //dynamixel read request
#ifdef CONFIG_EXAMPLES_DYNAMIXEL
    		 if(fd_dyn>=0){
    			 dynamixel_read_request(fd_dyn, 1,DYNAMIXEL_PERIODIC_READ_ADR, DYNAMIXEL_PERIODIC_READ_LEN);
    		 }
#endif

    		 if(fd_imu>=0){
    		 read(fd_imu,&imu_data,sizeof(imu_data));

   			 //clock_gettime(CLOCK_REALTIME, &imu_timestamp);
   			 //timestamp workaround:
   			 imu_data.timestamp_s=(int32_t)timestamp.tv_sec;
   			 imu_data.timestamp_ns=(int32_t)timestamp.tv_nsec;
   			 imu_data.prod_id=prod_id;
   			 mq_send(mqd_imu,(char*)&imu_data,sizeof(imu_data),1);

    		 }
   			 //read pps timestamp, control time
#ifdef PROFILING
clock_gettime(CLOCK_REALTIME, &time_c[i_time]);
#endif
    		 slice=false;
    	 } else {





#ifdef CONFIG_EXAMPLES_DYNAMIXEL
    		 //dynamixel read rx-buffer
    		 if(fd_dyn>=0){
    			 uint8_t rx_id;
				 uint8_t err;
				 bool new_data=0;
    			 ret= dynamixel_rx(fd_dyn, &dynamixel_data[8], DYNAMIXEL_PERIODIC_READ_LEN, &rx_id, &err);
    			 if (!ret){
    				 *(int32_t*)(&dynamixel_data[0])=(int32_t)timestamp.tv_sec;
    				 *(int32_t*)(&dynamixel_data[4])=(int32_t)timestamp.tv_nsec;
    				 new_data=true;
    			 //} else {
    			 //	 printf("E\n");
    			 }
    			 ret=mq_receive(mqd_dynamixel_cmd,dynamixel_cmd_data,sizeof(dynamixel_cmd_data),NULL);
    			 if(ret==4){
    				 switch (dynamixel_cmd_data[1]){
    				 case 1:
    					 dynamixel_write8(fd_dyn,1,dynamixel_cmd_data[0],dynamixel_cmd_data[2]);
    					 break;

    				 case 2:
    					 dynamixel_write16(fd_dyn,1,dynamixel_cmd_data[0],*(uint16_t*)(&dynamixel_cmd_data[2]));
    					 break;

    				 }
    			 }

    			 if(new_data){
    			   mq_send(mqd_dynamixel,dynamixel_data,sizeof(dynamixel_data),1);
    			 }
    		 }
#endif

#ifdef CONFIG_EXAMPLES_DYNAMIXEL
    		 int len;
    		 len=read(fd_hptc,&ic_data,sizeof(ic_data));

    		 if(len==sizeof(ic_data)){
//    			 printf("ch: %hhu: %d\n",ic_data.channel, ic_data.nsec);
//    			 if((ic_data.channel==0) && (!ic_data.flags)){
    			 if(!ic_data.flags){
    				 *(int32_t*)(&pulse_ts_data[0])=ic_data.sec;
    				 *(int32_t*)(&pulse_ts_data[4])=ic_data.nsec;
    				 pulse_ts_data[8]=ic_data.channel;
    				 pulse_ts_data[9]=ic_data.flags;
      			     mq_send(mqd_pulse_ts,pulse_ts_data,sizeof(pulse_ts_data),1);
    			 }
    		 }
#endif


#ifdef PROFILING
clock_gettime(CLOCK_REALTIME, &time_d[i_time]);
#endif

#if 0
if (i==99){
	struct timespec tnow;
	i=0;

	iax=imu_data.x_accl;
	iay=imu_data.y_accl;
	iaz=imu_data.z_accl;

	clock_gettime(CLOCK_REALTIME,&tnow);

	printf("Time triggered: %d.%09ds\n",timestamp.tv_sec,timestamp.tv_nsec);
	printf("Time now     :  %d.%09ds\n",tnow.tv_sec,tnow.tv_nsec);

	//printf("%4d ax: %8.4lf ay: %8.4lf az: %8.4lf\n",i, 0.122e-7*(double)iax, 0.122e-7*(double)iay, 0.122e-7*(double)iaz);


	//printf("mq_send: %d\n", ret);
} else {
	i++;
}
#endif

    		 //trigger_nutros_tx

    		    int sem_val=0;
    		    sem_getvalue(sem_nutros_tx,&sem_val);
    		    if(sem_val<1)sem_post(sem_nutros_tx);

#ifdef CONFIG_EXAMPLES_SENSOR
    		    {
    		    	static int divider = 20;
    		    	divider--;
    		    	if(!divider){
    	    		    int sem_val=0;
    	    		    sem_getvalue(sem_uavcan,&sem_val);
    	    		    if(sem_val<1){
    	    		    	sem_post(sem_uavcan);
//    	    		    } else {
//    	    		    	printf("can busy!\n");
    	    		    }
    		    		divider=20;
    		    	}
    		    }
#endif

#ifdef PROFILING
clock_gettime(CLOCK_REALTIME, &time_d[i_time]);
#endif




    		    slice=true;
    	 }

#ifdef PROFILING
//clock_gettime(CLOCK_REALTIME, &time_c[i_time]);
{
    i_time++;
	if(i_time==100){
		i_time=0;
		for (int i=0;i<100;i++){
			printf("%3d: trig: %09d.%06d b: %06d c: %06d  d: %06d \n",i,time_a[i].tv_sec,time_a[i].tv_nsec/1000,time_b[i].tv_nsec/1000-time_a[i].tv_nsec/1000,time_c[i].tv_nsec/1000-time_b[i].tv_nsec/1000,time_d[i].tv_nsec/1000-time_b[i].tv_nsec/1000);
		}
	}


}
#endif




     }
  //never reached
  return 0;
}
