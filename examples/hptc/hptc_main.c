/****************************************************************************
 * examples/hptc/hptc_main.c
 *
 *   Copyright (C) 2008, 2011-2012 Gregory Nutt. All rights reserved.
 *   Author: Gregory Nutt <gnutt@nuttx.org>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>

#include <sys/ioctl.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <signal.h>
#include <errno.h>
#include <mqueue.h>
#include <nuttx/timers/timer.h>

#include <semaphore.h>
#include <mqueue.h>

#include <nuttx/timers/hptc_io.h>
#include <nuttx/timers/hptc.h>
/*IMU*/
//#include <nuttx/sensors/adis16xxx.h>
/*led*/
#include <nuttx/leds/userled.h>


/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

//#define CONFIG_COSMIC_SCHEDULER_DEVNAME "/dev/timer12"
//#define CONFIG_COSMIC_SCHEDULER_INTERVAL 10000
//#define CONFIG_COSMIC_SCHEDULER_SIGNO 17
//#define CONFIG_COSMIC_SCHEDULER_DELAY 10000

//#define CHILD_ARG ((void*)0x12345678)

//int child_apid;

#undef PROFILING

#ifdef PROFILING
struct timespec time_b[100];
struct timespec time_a[100];
struct timespec time_c[100];
struct timespec time_d[100];

int i_time=0;
#endif

/****************************************************************************
 * Public Functions
 ****************************************************************************/
//static volatile unsigned long g_nsignals;

/****************************************************************************
 * cosmic_scheduler_main
 ****************************************************************************/



#ifdef CONFIG_BUILD_KERNEL
int main(int argc, FAR char *argv[])
#else
int hptc_main(int argc, char *argv[])
#endif
{
#if 0
  struct timer_notify_s notify;
  struct sigaction act;
  struct timer_status_s status;
#endif
  int ret;
  int fd_hptc;
  struct hptc_ic_msg_s ic_data;
  sem_t* sem_hptc;


  FAR struct timespec timestamp;
//  FAR struct timespec semtimestamp;
//  pthread_attr_t attr;
//  pthread_t thread;


//  volatile uint32_t* gpiob_bssr=(void*)0x40020418;



#if 0
  int fd_led;
  userled_set_t supported;
  fd_led = open("/dev/userleds", O_WRONLY);
    if (fd_led < 0)
      {
        int errcode = errno;
        printf("led: ERROR: Failed to open %s: %d\n",
        		"/dev/userleds", errcode);

       sleep (2);
       return -1;
      }

    /* Get the set of LEDs supported */

    ret = ioctl(fd_led, ULEDIOC_SUPPORTED,
                (unsigned long)((uintptr_t)&supported));
    if (ret < 0)
      {
        int errcode = errno;
        printf("led_daemon: ERROR: ioctl(ULEDIOC_SUPPORTED) failed: %d\n",
               errcode);
        sleep(2);
        return -1;
      }

    /* Excluded any LEDs that not supported AND not in the set of LEDs the
     * user asked us to use.
     */

    printf("led: Supported LEDs 0x%02x\n", (unsigned int)supported);

    ret = ioctl(fd_led, ULEDIOC_SETALL, 0x01);

    sleep(2);

    ret = ioctl(fd_led, ULEDIOC_SETALL, 0x02);


    sleep(2);

    ret = ioctl(fd_led, ULEDIOC_SETALL, 0x00);

    sleep(2);
#endif

#if 0
    *gpiob_bssr=0x00000002;

    sleep(3);

    *gpiob_bssr=0x00000004;

    sleep(2);

    *gpiob_bssr=0x00060000;

    sleep(1);
    *gpiob_bssr=0x00000006;

    sleep(2);
#endif

  printf("hptc example\n");
  sleep(1);

  /* Open the timer device */

  printf("Open %s\n", CONFIG_HPTC_DEV_PATH);
sleep(1);

  fd_hptc = open(CONFIG_HPTC_DEV_PATH, O_RDONLY | O_NONBLOCK);
  if (fd_hptc < 0)
    {
	  fprintf(stderr, "ERROR: Failed to open %s: %d\n",
			  CONFIG_HPTC_DEV_PATH, errno);
      return EXIT_FAILURE;
    }



    printf("get hptc semaphore\n");
sleep(1);
    ret = ioctl(fd_hptc, HPTCIOC_GETSEM, (long int)&sem_hptc);
    if (ret < 0)
      {
        fprintf(stderr, "ERROR: Failed to get hptc semaphore: %d\n", errno);
        close(fd_hptc);
        return EXIT_FAILURE;
      }

//    int sem_val=0;
//    sem_getvalue(sem_hptc,&sem_val);
//    printf("sem: val=%d\n",sem_val);

    //------------------------------------------------------------------------------------------


     /* Start the timer */

     printf("Start the timer\n");
sleep(1);
     ret = ioctl(fd_hptc, HPTCIOC_START, 0);
     if (ret < 0)
       {
         fprintf(stderr, "ERROR: Failed to start the hptc: %d\n", errno);
         close(fd_hptc);
         return EXIT_FAILURE;
       }

printf("timer started.\n");
sleep(1);

#if 1
struct timespec ts1,ts2;
clock_gettime(CLOCK_REALTIME, &ts1);
clock_gettime(CLOCK_REALTIME, &ts2);

printf("ts1: %09d,%09d\n",ts1.tv_sec, ts1.tv_nsec);
printf("ts2: %09d,%09d\n",ts2.tv_sec, ts2.tv_nsec);
sleep(1);
#endif




 printf("run...\n");
 //sleep(2);

#if 1
     for(int i=0; i<10;i++){
    	 struct hptc_status_s hptc_status;
    	 ret = ioctl(fd_hptc, HPTCIOC_GETSTATUS, (long int) &hptc_status);
    	 printf("%2d:cnt:%10u; cnt1:%10u; cnt2:%10u\n", i,hptc_status.flags,hptc_status.cnt1,hptc_status.cnt2);
    	 sleep(1);
     }
#endif

     int i=0;

     for(;;){
    	 sem_wait(sem_hptc);


    	//clock_gettime(CLOCK_REALTIME, &timestamp);
        ret = ioctl(fd_hptc, HPTCIOC_GETSEMTIME, (long int)&timestamp);


        if (i==99){
	      struct timespec tnow;
	      i=0;


	      clock_gettime(CLOCK_REALTIME,&tnow);

	      printf("Time triggered: %d.%09ds\n",timestamp.tv_sec,timestamp.tv_nsec);
	      printf("Time now     :  %d.%09ds\n",tnow.tv_sec,tnow.tv_nsec);

        } else {
	      i++;
        }

     }
  //never reached
  return 0;
}
