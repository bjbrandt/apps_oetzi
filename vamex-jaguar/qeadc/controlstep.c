/*
 * controlstep.c
 *
 *  Created on: 05.07.2017
 *      Author: messBUS
 */

#include "control.h"
#include "stdio.h"  //snprintf, required??
//#include <stdlib.h> //...abs()

#define THROTTLE_MIN 0.04  //100 Hz:0.04
#define THROTTLE_SLOW 0.04

#define POSCTRL

#define eODOmax (20.0)
#define eODOmin ( -20.0 )

#define BREAK_POWER_SCALE (0.25)


//static float pPower=8.0;//100Hz:1.0;
//static float iPower=0.3;//100Hz: 0.05;
//static float dPower=0.15;

static float pPower=3.0;//100Hz:1.0;
static float iPower=0.05;//100Hz: 0.05;
static float dPower=0.00;



#define STOP_LEFT_FLAG 0x01
#define STOP_RIGHT_FLAG 0x02
#define STOP_DELTA_MIX 0.25


void SpeedControlSetPID(float p, float i, float d)
{
	pPower=p;
	iPower=i;
	dPower=d;
}
void SpeedControlGetPID(float *p, float *i, float *d)
{
	if(p!=NULL) *p=pPower;
	if(i!=NULL) *i=iPower;
	if(d!=NULL) *d=dPower;
}


void SpeedControlStep(float SpeedLeft, float SpeedRight, int16_t ODO_Cnt[4],  uint8_t disable_flag, int8_t *powersetting)
{


	static float eODO_delta[4]={0.0,0.0,0.0,0.0};
	static float eODO_delta1[4]={0.0,0.0,0.0,0.0};
	static float eODO_delta2[4]={0.0,0.0,0.0,0.0};
	static float uPower[4]={0.0,0.0,0.0,0.0};
	static int16_t ODO_Cnt_Last[4]={0,0,0,0};
	uint32_t stop_flag=0;

	float throttle_left,throttle_right;

	throttle_left=SpeedLeft;
	throttle_right=SpeedRight;

//	printf("2)l1000: %d ,r1000: %d\n",(int)(throttle_left*1000.0),(int)(throttle_right*1000.0));

	if((throttle_left<THROTTLE_MIN) && (throttle_left> (-THROTTLE_MIN))){
		throttle_left=0.0;
		stop_flag |= STOP_LEFT_FLAG;
	}

	if((throttle_right<THROTTLE_MIN) && (throttle_right> (-THROTTLE_MIN))){
		throttle_right=0.0;
		stop_flag |= STOP_RIGHT_FLAG;
	}





	//avoid opposite power at active stop -> equalize eODO_delta

	if(stop_flag & STOP_LEFT_FLAG){
		if((eODO_delta[0]*eODO_delta[1])<0.0){ //opposite error
			float eODO_delta_temp = eODO_delta[0];
			eODO_delta[0]=(1.0 - STOP_DELTA_MIX)*eODO_delta[0]+(STOP_DELTA_MIX)*eODO_delta[1];
			eODO_delta[1]=(1.0 - STOP_DELTA_MIX)*eODO_delta[1]+(STOP_DELTA_MIX)*eODO_delta_temp;
		}
	}

	if(stop_flag & STOP_RIGHT_FLAG){
		if((eODO_delta[2]*eODO_delta[3])<0.0){ //opposite error
			float eODO_delta_temp = eODO_delta[2];
			eODO_delta[2]=(1.0 - STOP_DELTA_MIX)*eODO_delta[2]+(STOP_DELTA_MIX)*eODO_delta[3];
			eODO_delta[3]=(1.0 - STOP_DELTA_MIX)*eODO_delta[3]+(STOP_DELTA_MIX)*eODO_delta_temp;
		}
	}





	int16_t mODO_delta[4];
	float   fmODO_delta[4];


	eODO_delta2[0]=eODO_delta1[0];
	eODO_delta2[1]=eODO_delta1[1];
	eODO_delta2[2]=eODO_delta1[2];
	eODO_delta2[3]=eODO_delta1[3];

	eODO_delta1[0]=eODO_delta[0];
	eODO_delta1[1]=eODO_delta[1];
	eODO_delta1[2]=eODO_delta[2];
	eODO_delta1[3]=eODO_delta[3];


	mODO_delta[0]=ODO_Cnt[0]-ODO_Cnt_Last[0];
	mODO_delta[1]=ODO_Cnt[1]-ODO_Cnt_Last[1];
	mODO_delta[2]=ODO_Cnt[2]-ODO_Cnt_Last[2];
	mODO_delta[3]=ODO_Cnt[3]-ODO_Cnt_Last[3];







	fmODO_delta[0]= mODO_delta[0];
	fmODO_delta[1]= mODO_delta[1];
	fmODO_delta[2]= mODO_delta[2];
	fmODO_delta[3]= mODO_delta[3];



//	printf("%05d %05d,%05d,%05d,Tl:%05d,Tr:%05d\n",mODO_delta[0],mODO_delta[1],mODO_delta[2],mODO_delta[3],(int)throttle_left,(int)throttle_right);



	float throttle_slave_left_lim=0.1; //allow up to 10% speed error to equalize power
	float throttle_slave_left=(uPower[0]-uPower[1])/128.0;
	if(throttle_slave_left > throttle_slave_left_lim){
		throttle_slave_left= throttle_slave_left_lim;
	} else if (throttle_slave_left < (-throttle_slave_left_lim)){
		throttle_slave_left= -throttle_slave_left_lim;
	}


	eODO_delta[0]+=throttle_left-fmODO_delta[0];
	eODO_delta[1]+=throttle_left*(1.0+throttle_slave_left)-fmODO_delta[1];


	if(eODO_delta[0]>eODOmax){
		eODO_delta[0]=eODOmax;
	} else if(eODO_delta[0]<eODOmin) {
		eODO_delta[0]=eODOmin;
	}

	if(eODO_delta[1]>eODOmax){
		eODO_delta[1]=eODOmax;
	} else if(eODO_delta[1]<eODOmin) {
		eODO_delta[1]=eODOmin;
	}



	float throttle_slave_right_lim=0.1; //allow up to 10% speed error to equalize power
	float throttle_slave_right=(uPower[2]-uPower[3])/128.0;
	if(throttle_slave_right > throttle_slave_right_lim){
		throttle_slave_right= throttle_slave_right_lim;
	} else if (throttle_slave_right < (-throttle_slave_right_lim)){
		throttle_slave_right= -throttle_slave_right_lim;
	}


	eODO_delta[2]+=throttle_right-fmODO_delta[2];
	eODO_delta[3]+=throttle_right*(1.0+throttle_slave_right)-fmODO_delta[3];



	if(eODO_delta[2]>eODOmax){
		eODO_delta[2]=eODOmax;
	} else if(eODO_delta[2]<eODOmin) {
		eODO_delta[2]=eODOmin;
	}

	if(eODO_delta[3]>eODOmax){
		eODO_delta[3]=eODOmax;
	} else if(eODO_delta[3]<eODOmin) {
		eODO_delta[3]=eODOmin;
	}


	uPower[0] = uPower[0] + pPower * (eODO_delta[0]-eODO_delta1[0])
				        		  + iPower * eODO_delta1[0]
														 + dPower * (eODO_delta[0] - 2.0* eODO_delta1[0] + eODO_delta2[0]);


	uPower[1] = uPower[1] + pPower * (eODO_delta[1]-eODO_delta1[1])
				        		  + iPower * eODO_delta1[1]
														 + dPower * (eODO_delta[1] - 2.0* eODO_delta1[1] + eODO_delta2[1]);

	uPower[2] = uPower[2] + pPower * (eODO_delta[2]-eODO_delta1[2])
				        		  + iPower * eODO_delta1[2]
														 + dPower * (eODO_delta[2] - 2.0* eODO_delta1[2] + eODO_delta2[2]);

	uPower[3] = uPower[3] + pPower * (eODO_delta[3]-eODO_delta1[3])
				        		  + iPower * eODO_delta1[3]
														 + dPower * (eODO_delta[3] - 2.0* eODO_delta1[3] + eODO_delta2[3]);



	//limit throttle LH
	if(throttle_left>THROTTLE_MIN){ //forward
		if(uPower[0] > 127.5){
			uPower[0]=127.5;
		} else if (uPower[0] < -0.0){
			uPower[0]*=BREAK_POWER_SCALE;
			if (uPower[0]<-127.5){
				uPower[0]=-127.5;
			}
		}
		if(uPower[1] > 127.5){
			uPower[1]=127.5;
		} else if (uPower[1] < 0.0){
			uPower[1]*=BREAK_POWER_SCALE;
			if (uPower[1]<-127.5){
				uPower[1]=-127.5;
			}
		}
	} else if(throttle_left < -(THROTTLE_MIN)){ //backward
		if(uPower[0] > 0.0){
			uPower[0]*=BREAK_POWER_SCALE;
			if (uPower[0]>127.5){
				uPower[0]=127.5;
			}
		} else if (uPower[0] < -127.5){
			uPower[0]=-127.5;
		}
		if(uPower[1] > 0.0){
			uPower[1]*=BREAK_POWER_SCALE;
			if (uPower[1]>127.5){
				uPower[1]=127.5;
			}
		} else if (uPower[1] < -127.5){
			uPower[1]=-127.5;
		}
	} else { //stop
#if 0
		eODO_delta[0]=eODO_delta1[0]=eODO_delta2[0]=0.0;
		eODO_delta[1]=eODO_delta1[1]=eODO_delta2[1]=0.0;
		uPower[0]=uPower[1]=0.0;

#else
		//active stop
		if(uPower[0] > 127.5){
			uPower[0]=127.5;
		} else if (uPower[0] < -127.5){
			uPower[0]=-127.5;
		}
		if(uPower[1] > 127.5){
			uPower[1]=127.5;
		} else if (uPower[1] < -127.5){
			uPower[1]=-127.5;
		}
#endif




	}


	//limit throttle RH
	if(throttle_right>THROTTLE_MIN){ //forward
		if(uPower[2] > 127.5){
			uPower[2]=127.5;
		} else if (uPower[2] < -0.0){
			uPower[2]*=BREAK_POWER_SCALE;
			if (uPower[2]<-127.5){
				uPower[2]=-127.5;
			}
		}
		if(uPower[3] > 127.5){
			uPower[3]=127.5;
		} else if (uPower[3] < 0.0){
			uPower[3]*=BREAK_POWER_SCALE;
			if (uPower[3]<-127.5){
				uPower[3]=-127.5;
			}
		}
	} else if(throttle_right < -(THROTTLE_MIN)){ //backward
		if(uPower[2] > 0.0){
			uPower[2]*=BREAK_POWER_SCALE;
			if (uPower[2]>127.5){
				uPower[2]=127.5;
			}
		} else if (uPower[2] < -127.5){
			uPower[2]=-127.5;
		}
		if(uPower[3] > 0.0){
			uPower[3]*=BREAK_POWER_SCALE;
			if (uPower[3]>127.5){
				uPower[3]=127.5;
			}
		} else if (uPower[3] < -127.5){
			uPower[3]=-127.5;
		}
	} else { //stop

#if 0
		//passiv stop
		eODO_delta[2]=eODO_delta1[2]=eODO_delta2[2]=0.0;
		eODO_delta[3]=eODO_delta1[3]=eODO_delta2[3]=0.0;
		uPower[2]=uPower[3]=0.0;
#else
		//active stop
		if(uPower[2] > 127.5){
			uPower[2]=127.5;
		} else if (uPower[2] < -127.5){
			uPower[2]=-127.5;
		}
		if(uPower[3] > 127.5){
			uPower[3]=127.5;
		} else if (uPower[3] < -127.5){
			uPower[3]=-127.5;
		}
#endif
	}


	//avoid opposite power at active stop

	if(stop_flag & STOP_LEFT_FLAG){
		if((uPower[0]*uPower[1])<-0.5){ //opposite error
			float uPower_temp= uPower[0];
			uPower[0]=(1.0 - STOP_DELTA_MIX)*uPower[0]+(STOP_DELTA_MIX)*uPower[1];
			uPower[1]=(1.0 - STOP_DELTA_MIX)*uPower[1]+(STOP_DELTA_MIX)*uPower_temp;
		}
	}



	if(stop_flag & STOP_RIGHT_FLAG){
		if((uPower[2]*uPower[3])<-0.5){ //opposite error
			float uPower_temp= uPower[2];
			uPower[2]=(1.0 - STOP_DELTA_MIX)*uPower[2]+(STOP_DELTA_MIX)*uPower[3];
			uPower[3]=(1.0 - STOP_DELTA_MIX)*uPower[3]+(STOP_DELTA_MIX)*uPower_temp;
		}
	}





	if(disable_flag){
		eODO_delta[0]=eODO_delta1[0]=eODO_delta2[0]=0.0;
		eODO_delta[1]=eODO_delta1[1]=eODO_delta2[1]=0.0;
		uPower[0]=uPower[1]=0.0;
		eODO_delta[2]=eODO_delta1[2]=eODO_delta2[2]=0.0;
		eODO_delta[3]=eODO_delta1[3]=eODO_delta2[3]=0.0;
		uPower[2]=uPower[3]=0.0;

	}


	powersetting[0]=uPower[0];
	powersetting[1]=uPower[1];
	powersetting[2]=uPower[2];
	powersetting[3]=uPower[3];

	ODO_Cnt_Last[0]=ODO_Cnt[0];
	ODO_Cnt_Last[1]=ODO_Cnt[1];
	ODO_Cnt_Last[2]=ODO_Cnt[2];
	ODO_Cnt_Last[3]=ODO_Cnt[3];
}
